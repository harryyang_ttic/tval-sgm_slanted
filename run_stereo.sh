#!/bin/sh

./bin/sgmstereo ./sample_data/000002_10L.png ./sample_data/000002_10R.png -o ./sample_data/result_stereo
./bin/stereoslic ./sample_data/000002_10L.png ./sample_data/000002_10R.png ./sample_data/result_stereo/000002_10L_left_disparity.png ./sample_data/result_stereo/000002_10R_right_disparity.png -o ./sample_data/result_stereo/000002_10L_slic.png
