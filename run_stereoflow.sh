#!/bin/sh

./bin/estalpha ./sample_data/result_stereo/000002_10L_left_disparity.png ./sample_data/result_flow/000002_flow.png ./sample_data/000002.txt ./sample_data/result_flow/000002_fund.dat ./sample_data/result_stereoflow/000002_mot.dat
./bin/sgmstereoflow ./sample_data/000002_10L.png ./sample_data/000002_10R.png ./sample_data/000002_11L.png ./sample_data/result_stereo/000002_10L_left_disparity.png ./sample_data/result_flow/000002_flow_first_vz.png ./sample_data/result_stereoflow/000002_mot.dat -o ./sample_data/result_stereoflow
./bin/smoothfit ./sample_data/000002_10L.png ./sample_data/result_stereoflow/000002_10L_left_disparity.png ./sample_data/result_stereoflow/000002_mot.dat -o ./sample_data/result_stereoflow/000002_smfit.png
