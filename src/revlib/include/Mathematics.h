#ifndef REVLIB_MATHEMATICS_H
#define REVLIB_MATHEMATICS_H

#include <cmath>

namespace rev {
    
const double REV_PI = 3.1415926535897932384626433832795;
const float EpsilonFloat = 1.19209290E-07F;
const double EpsilonDouble = 2.220446049250313e-16;
    
    
inline float fastApproximateSqrtf(const float x) {
    union {
        float x;
        int i;
    } u;
        
    float halfX = 0.5f*x;
        
    u.x = x;
    u.i = 0x5f3759df - (u.i >> 1);
        
    // Two Newton steps
    u.x = u.x*(1.5f - halfX*u.x*u.x);
    u.x = u.x*(1.5f - halfX*u.x*u.x);
        
    return u.x;
}
    
inline double fastApproximateSqrt(const double x) {
    union {
        double x;
        long long i;
    } u;
        
    double halfX = x*0.5;
        
    u.x = x;
    u.i = 0x5fe6ec85e7de30daLL - (u.i >> 1) ;
        
    // Two Newton steps
    u.x = u.x*(1.5 - halfX*u.x*u.x);
    u.x = u.x*(1.5 - halfX*u.x*u.x);
        
    return u.x;
}
    
inline float fastSqrtf(const float x) {
    return (x < 1e-8) ? 0 : x*fastApproximateSqrtf(x);
}
    
inline double fastSqrt(const double x) {
    return (x < 1e-8) ? 0 : x*fastApproximateSqrt(x);
}
    
inline float fastAtan2f(const float y, const float x) {
    float absoluteY = fabsf(y) + EpsilonFloat;
        
    double r, angle;
    if (x >= 0) {
        r = (x - absoluteY)/(x + absoluteY);
        angle = static_cast<float>(REV_PI/4);
    } else {
        r = (x + absoluteY)/(absoluteY - x);
        angle = static_cast<float>(3*REV_PI/4);
    }
    angle += (0.1821f*r*r - 0.9675f)*r;
        
    return (y < 0) ? -angle : angle;
}
    
inline double fastAtan2(const double y, const double x) {
    double absoluteY = fabs(y) + EpsilonDouble;
        
    double r, angle;
    if (x >= 0) {
        r = (x - absoluteY)/(x + absoluteY);
        angle = REV_PI/4;
    } else {
        r = (x + absoluteY)/(absoluteY - x);
        angle = 3*REV_PI/4;
    }
    angle += (0.1821*r*r - 0.9675)*r;
        
    return (y < 0) ? -angle : angle;
}
    
inline float mod2Pif(const float x) {
    float modX = x;
    while (modX > static_cast<float>(2*REV_PI)) modX -= static_cast<float>(2*REV_PI);
    while (modX < 0.0f) modX += static_cast<float>(2*REV_PI);
    
    return modX;
}

}

#endif
