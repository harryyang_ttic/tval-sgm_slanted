#ifndef REVLIB_KEYPOINT_H
#define REVLIB_KEYPOINT_H

namespace rev {

class Keypoint {
public:
    Keypoint() : x_(-1), y_(-1), scale_(0), orientation_(-1), score_(0) {}
    Keypoint(const double x, const double y) : x_(x), y_(y), scale_(0), orientation_(-1), score_(0) {}
    
    void clear() { x_ = -1; y_ = -1; scale_ = 0; }
    void setPosition(const double x, const double y) { x_ = x; y_ = y; }
    void setScale(const double scale) { scale_ = scale; }
    void setOrientation(const double orientation) { orientation_ = orientation; }
    void setScore(const double score) { score_ = score; }
    
    double x() const { return x_; }
    double y() const { return y_; }
    double scale() const { return scale_; }
    double orientation() const { return orientation_; }
    double score() const { return score_; }
    
private:
    double x_;
    double y_;
    double scale_;
    double orientation_;
    double score_;
};
    
}

#endif
