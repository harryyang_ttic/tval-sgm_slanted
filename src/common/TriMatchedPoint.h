#pragma once

#include <revlib.h>

class TriMatchedPoint {
public:
	TriMatchedPoint() : firstLeftX_(-1), firstLeftY_(-1), firstRightX_(-1), firstRightY_(-1), secondLeftX_(-1), secondLeftY_(-1) {}
	TriMatchedPoint(const double firstLeftX, const double firstLeftY,
		const double firstRightX, const double firstRightY,
		const double secondLeftX, const double secondLeftY)
		: firstLeftX_(firstLeftX), firstLeftY_(firstLeftY),
		firstRightX_(firstRightX), firstRightY_(firstRightY),
		secondLeftX_(secondLeftX), secondLeftY_(secondLeftY) {}
	TriMatchedPoint(const rev::Keypoint& firstLeftKeypoint, const rev::Keypoint& firstRightKeypoint, const rev::Keypoint& secondLeftKeypoint)
		: firstLeftX_(firstLeftKeypoint.x()), firstLeftY_(firstLeftKeypoint.y()),
		firstRightX_(firstRightKeypoint.x()), firstRightY_(firstRightKeypoint.y()),
		secondLeftX_(secondLeftKeypoint.x()), secondLeftY_(secondLeftKeypoint.y()) {}

	void set(const double firstLeftX, const double firstLeftY,
		const double firstRightX, const double firstRightY,
		const double secondLeftX, const double secondLeftY)
	{
		firstLeftX_ = firstLeftX;  firstLeftY_ = firstLeftY;
		firstRightX_ = firstRightX;  firstRightY_ = firstRightY;
		secondLeftX_ = secondLeftX;  secondLeftY_ = secondLeftY;
	}
	void set(const rev::Keypoint& firstLeftKeypoint, const rev::Keypoint& firstRightKeypoint, const rev::Keypoint& secondLeftKeypoint) {
		firstLeftX_ = firstLeftKeypoint.x();  firstLeftY_ = firstLeftKeypoint.y();
		firstRightX_ = firstRightKeypoint.x();  firstRightY_ = firstRightKeypoint.y();
		secondLeftX_ = secondLeftKeypoint.x();  secondLeftY_ = secondLeftKeypoint.y();
	}
	void setWorldCoordinates(const double worldX, const double worldY, const double worldZ) {
		worldX_ = worldX;
		worldY_ = worldY;
		worldZ_ = worldZ;
	}

	double firstLeftX() const { return firstLeftX_; }
	double firstLeftY() const { return firstLeftY_; }
	double firstRightX() const { return firstRightX_; }
	double firstRightY() const { return firstRightY_; }
	double secondLeftX() const { return secondLeftX_; }
	double secondLeftY() const { return secondLeftY_; }

	double worldX() const { return worldX_; }
	double worldY() const { return worldY_; }
	double worldZ() const { return worldZ_; }

private:
	double firstLeftX_;
	double firstLeftY_;
	double firstRightX_;
	double firstRightY_;
	double secondLeftX_;
	double secondLeftY_;

	double worldX_;
	double worldY_;
	double worldZ_;
};
