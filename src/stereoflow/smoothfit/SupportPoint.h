#ifndef SUPPORTPOINT_H
#define SUPPORTPOINT_H

#include <string>
#include <vector>

class SupportPoint {
public:
    SupportPoint() : x_(-1), y_(-1) {}
    SupportPoint(const int x, const int y) : x_(x), y_(y) {}
	
    void clear();
    void set(const int x, const int y) { x_ = x; y_ = y; }
    void appendDisparity(const double disparity, const double error = 0);
    
    int x() const { return x_; }
    int y() const { return y_; }
    int disparityTotal() const { return static_cast<int>(disparities_.size()); }
    double disparity(const int candidateIndex = 0) const;
    double disparityError(const int candidateIndex = 0) const;
    
private:
    struct DisparityCandidate {
        double disparity;
        double error;
        
        bool operator<(const DisparityCandidate& comparisonDisparityCandidate) const;
    };
    
    int x_;
    int y_;
    std::vector<DisparityCandidate> disparities_;
};

std::vector<SupportPoint> readSupportPointFile(const std::string& filename);
void writeSupportPointFile(const std::string& filename, const std::vector<SupportPoint>& supportPoints);

#endif
