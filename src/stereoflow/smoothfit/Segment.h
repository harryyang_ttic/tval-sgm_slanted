#ifndef SEGMENT_H
#define SEGMENT_H

#include <vector>
#include "SupportPoint.h"

class Segment {
public:
    Segment() : centerX_(-1), centerY_(-1) {}
    
    void clear();
    void setCenter(const double centerX, const double centerY) { centerX_ = centerX; centerY_ = centerY; }
    void appendInteriorPoint(const SupportPoint& interiorPoint) { interiorPoints_.push_back(interiorPoint); }
    void appendBoundaryIndex(const int boundaryIndex) { boundaryIndices_.push_back(boundaryIndex); }
    void appendSegmentPixel(const int x, const int y) { segmentPixelXs_.push_back(x); segmentPixelYs_.push_back(y); }
    
    double centerX() const { return centerX_; }
    double centerY() const { return centerY_; }
	
    int interiorPointTotal() const { return static_cast<int>(interiorPoints_.size()); }
    SupportPoint interiorPoint(const int index) const;
    
    int neighborTotal() const { return static_cast<int>(neighborSegmentIndices_.size()); }
    int neighborIndex(const int index) const;
    
    int boundaryTotal() const { return static_cast<int>(boundaryIndices_.size()); }
    int boundaryIndex(const int index) const;
    
    int segmentPixelTotal() const { return static_cast<int>(segmentPixelXs_.size()); }
    int segmentPixelX(const int index) const;
    int segmentPixelY(const int index) const;
    
    void setInteriorPointDisparity(const int pointIndex, const double disparity);
    
private:
    double centerX_;
    double centerY_;
    std::vector<SupportPoint> interiorPoints_;
    std::vector<int> neighborSegmentIndices_;
    std::vector<int> boundaryIndices_;
    std::vector<int> segmentPixelXs_;
    std::vector<int> segmentPixelYs_;
};

class SegmentBoundary {
public:
    SegmentBoundary();
    SegmentBoundary(const int firstSegmentIndex, const int secondSegmentIndex);
    
    void set(const int firstSegmentIndex, const int secondSegmentIndex);
    void appendBoundaryPoint(const SupportPoint& boundaryPoint) { boundaryPoints_.push_back(boundaryPoint); }
    void appendBoundaryPixel(const double x, const double y) { boundaryPixelXs_.push_back(x); boundaryPixelYs_.push_back(y); }
    
    int segmentIndex(const int index) const;
    bool consistOf(const int firstSegmentIndex, const int secondSegmentIndex) const;
    int include(const int segmentIndex) const;
    int boundaryPointTotal() const { return static_cast<int>(boundaryPoints_.size()); }
    SupportPoint boundaryPoint(const int index) const;
    int boundaryPixelTotal() const { return static_cast<int>(boundaryPixelXs_.size()); }
    double boundaryPixelX(const int index) const;
    double boundaryPixelY(const int index) const;
    
    void setBoundaryPointDisparity(const int pointIndex, const double disparity);
    
private:
    int segmentIndices_[2];
    std::vector<SupportPoint> boundaryPoints_;
    std::vector<double> boundaryPixelXs_;
    std::vector<double> boundaryPixelYs_;
};

class SegmentJunction {
public:
    SegmentJunction();
    SegmentJunction(const int x, const int y);
    
    void setPosition(const int x, const int y);
    void setSegmentIndices(const std::vector<int>& threeSegmentIndices);
    void setBoundaryIndices(const std::vector<int>& threeBoundaryIndices);
    void appendJunctionPoint(const SupportPoint& junctionPoint) { junctionPoints_.push_back(junctionPoint); }
    
    int x() const { return x_; }
    int y() const { return y_; }
    int segmentIndex(const int index) const;
    bool consistOf(const std::vector<int>& threeSegmentIndices) const;
    int boundaryIndex(const int index) const;
    int junctionPointTotal() const { return static_cast<int>(junctionPoints_.size()); }
    SupportPoint junctionPoint(const int index) const;
    
    void setJunctionPointDisparity(const int pointIndex, const double disparity);
    
private:
    int x_;
    int y_;
    int segmentIndices_[3];
    int boundaryIndices_[3];
    std::vector<SupportPoint> junctionPoints_;
    
};

class SegmentCrossJunction {
public:
    SegmentCrossJunction();
    SegmentCrossJunction(const int x, const int y);
    
    void setPosition(const int x, const int y);
    void setSegmentIndices(const std::vector<int>& fourSegmentIndices);
    void setBoundaryIndices(const std::vector<int>& fourBoundaryIndices, const std::vector<bool>& fourBoundaryDirections);
    void appendJunctionPoint(const SupportPoint& junctionPoint) { junctionPoints_.push_back(junctionPoint); }
    
    int x() const { return x_; }
    int y() const { return y_; }
    int segmentIndex(const int index) const;
    bool consistOf(const std::vector<int>& fourSegmentIndices) const;
    int boundaryIndex(const int index) const;
    bool boundaryDirection(const int index) const;
    int junctionPointTotal() const { return static_cast<int>(junctionPoints_.size()); }
    SupportPoint junctionPoint(const int index) const;
	
    void setJunctionPointDisparity(const int pointIndex, const double disparity);
    
private:
    int x_;
    int y_;
    int segmentIndices_[4];
    int boundaryIndices_[4];
    bool boundaryDirections_[4];
    std::vector<SupportPoint> junctionPoints_;
    
};

#endif
