#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>
#include <cmdline.h>
#include <revlib.h>
#include "CameraMotion.h"
#include "Smoothfit.h"

struct ParameterStereoSlic {
	bool verbose;
	double disparityFactor;
	int superpixelTotal;
	std::string firstLeftImageFilename;
	std::string firstLeftDisparityImageFilename;
	std::string cameraMotionFilename;
	std::string outputSegmentImageFilename;
	std::string outputBoundaryImageFilename;
	std::string outputDisparityImageFilename;
	std::string outputFlowImageFilename;
	std::string outputBoundaryLabelImageFilename;
	std::string outputBoundaryLabelFilename;
	std::string outputDisparityPlaneFilename;
	double fx;
	double fy;
	double ox;
	double oy;
	double B;
};

// Prototype declaration
ParameterStereoSlic parseCommandline(int argc, char* argv[]);

ParameterStereoSlic parseCommandline(int argc, char* argv[]) {
	// Make command parser
	cmdline::parser commandParser;
	commandParser.add<std::string>("output", 'o', "output file", false, "");
	commandParser.add<double>("factor", 'f', "disparity factor of input disparity image", false, 256.0);
	commandParser.add<int>("superpixel", 's', "the number of superpixels", false, 2000);
	commandParser.add<double>("fx",'x',"focal length x",false,1019.304708045163);
	commandParser.add<double>("fy",'y',"focal length y",false,1019.304708045163);
	commandParser.add<double>("ox",'w',"focal origin x",false,789.3798370361328);
	commandParser.add<double>("oy",'z',"focal origin y",false,612.2119750976562);
	commandParser.add<double>("baseline",'b',"baseline b",false,0.558253);
	commandParser.add("verbose", 'v', "verbose");
	commandParser.add("help", 'h', "display this message");
	commandParser.set_program_name("smoothfit");
	commandParser.footer("imgL0 dispL0 motion");

	// Parse command line
	bool isCorrectCommandline = commandParser.parse(argc, argv);
	// Check arguments
	if (!isCorrectCommandline) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommandline || commandParser.exist("help") || commandParser.rest().size() < 3) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	// Set program parameters
	ParameterStereoSlic parameters;
	// Verbose flag
	parameters.verbose = commandParser.exist("verbose");
	// Disparity factor
	parameters.disparityFactor = commandParser.get<double>("factor");
	// The number of superpixels
	parameters.superpixelTotal = commandParser.get<int>("superpixel");
	parameters.fx=commandParser.get<double>("fx");
	parameters.fy=commandParser.get<double>("fy");
	parameters.ox=commandParser.get<double>("ox");
	parameters.oy=commandParser.get<double>("oy");
	parameters.B=commandParser.get<double>("baseline");

	// Input files
	parameters.firstLeftImageFilename = commandParser.rest()[0];
	parameters.firstLeftDisparityImageFilename = commandParser.rest()[1];
	parameters.cameraMotionFilename = commandParser.rest()[2];
	// Output files
	std::string outputSegmentImageFilename = commandParser.get<std::string>("output");
	if (outputSegmentImageFilename == "") {
		outputSegmentImageFilename = parameters.firstLeftImageFilename;
		size_t dotPosition = outputSegmentImageFilename.rfind('.');
		if (dotPosition != std::string::npos) outputSegmentImageFilename.erase(dotPosition);
		outputSegmentImageFilename += "_seg.png";
	}
	parameters.outputSegmentImageFilename = outputSegmentImageFilename;
	std::string outputBoundaryImageFilename = outputSegmentImageFilename;
	size_t dotPosition = outputBoundaryImageFilename.rfind('.');
	if (dotPosition != std::string::npos) outputBoundaryImageFilename.erase(dotPosition);
	std::string outputDisparityImageFilename = outputBoundaryImageFilename;
	std::string outputFlowImageFilename = outputBoundaryImageFilename;
	std::string outputBoundaryLabelImageFilename = outputBoundaryImageFilename;
	std::string outputBoundaryLabelFilename = outputBoundaryImageFilename;
	//by harry
	std::string outputDisparityPlaneFilename = outputBoundaryImageFilename;

	outputBoundaryImageFilename += "_boundary.png";
	outputDisparityImageFilename += "_disparity.png";
	outputFlowImageFilename += "_flow.png";
	outputBoundaryLabelImageFilename += "_label.png";
	outputBoundaryLabelFilename += "_label.txt";
	outputDisparityPlaneFilename += "_planes.txt";

	parameters.outputBoundaryImageFilename = outputBoundaryImageFilename;
	parameters.outputDisparityImageFilename = outputDisparityImageFilename;
	parameters.outputFlowImageFilename = outputFlowImageFilename;
	parameters.outputBoundaryLabelImageFilename = outputBoundaryLabelImageFilename;
	parameters.outputBoundaryLabelFilename = outputBoundaryLabelFilename;
    parameters.outputDisparityPlaneFilename = outputDisparityPlaneFilename;

	return parameters;
}

void writeBoundaryLabelFile(const std::string boundaryLabelFilename, const std::vector< std::vector<int> >& boundaryLabelList) {
	std::ofstream outputFileStream(boundaryLabelFilename.c_str(), std::ios_base::out);
	if (outputFileStream.fail()) {
		std::cerr << "error: can't open file (" << boundaryLabelFilename << ")" << std::endl;
		exit(1);
	}

	for (int i = 0; i < boundaryLabelList.size(); ++i) {
		outputFileStream << boundaryLabelList[i][0] << " ";
		outputFileStream << boundaryLabelList[i][1] << " ";
		outputFileStream << boundaryLabelList[i][2] << std::endl;
	}

	outputFileStream.close();
}

//by harry
void writeDisparityPlaneFile(const std::string disparityPlaneFileName, const std::vector< std::vector<double> >& disparityPlanes, const CameraMotion& cameraMotion,
double fx, double fy, double ox, double oy, double B){
    std::ofstream outputFileStream(disparityPlaneFileName.c_str(), std::ios_base::out);
    if(outputFileStream.fail()){
		std::cerr << "error: can't open file (" << disparityPlaneFileName << ")" << std::endl;
		exit(1);
	}
    Eigen::Matrix3d calibMatrix=cameraMotion.calibrationMatrix();
    //double fx=calibMatrix(0,0), fy=calibMatrix(1,1), ox=calibMatrix(0,2), oy=calibMatrix(1,2);
    //const double fx=721.5377, fy=7.215377e+02, ox=609.5593, oy=1.728540e+02, B=5.370000e-01;
    //const double fx=1019.304708045163, fy=1019.304708045163, ox=789.3798370361328, oy=612.2119750976562, B=0.558253;
    outputFileStream<<"camera intrinsic parameters (fx, fy, ox, oy, baseline): "<<fx<<" "<<fy<<" "<<ox<<" "<<oy<<" "<<B<<std::endl;
	for(int i=0;i<disparityPlanes.size();i++){
        outputFileStream << i <<" ";
        double a=disparityPlanes[i][0], b=disparityPlanes[i][1], c=disparityPlanes[i][2];
        outputFileStream << a << " " <<b<<" "<<"-1"<<" "<<c<<" ";
        double plane_a=fx*a/(fx*B),
        plane_b=fy*b/(fx*B),
        plane_c=(a*ox+b*oy+c)/(fx*B);
        outputFileStream << plane_a << " " << plane_b <<" "<<plane_c<<" "<<"-1"<<" ";
        double outlier=disparityPlanes[i][3];
        if(outlier==0)
            outputFileStream<<"N"<<std::endl;
        else
            outputFileStream<<"Y"<<std::endl;
	}
	outputFileStream.close();
}

std::string getFilenameExtension(const std::string filename) {
    std::string filenameExtension = filename;
    size_t dotPosition = filenameExtension.rfind('.');
    if (dotPosition != std::string::npos) {
        filenameExtension.erase(0, dotPosition+1);
    } else {
        filenameExtension = "";
    }

    return filenameExtension;
}


int main(int argc, char* argv[]) {
	// Parse command line
	ParameterStereoSlic parameters = parseCommandline(argc, argv);

	// Output parameters
	if (parameters.verbose) {
		std::cerr << std::endl;
		std::cerr << "First left image:       " << parameters.firstLeftImageFilename << std::endl;
		std::cerr << "First left disparity:   " << parameters.firstLeftDisparityImageFilename << std::endl;
		std::cerr << "Camera motion:          " << parameters.cameraMotionFilename << std::endl;
		std::cerr << "Output segment image:   " << parameters.outputSegmentImageFilename << std::endl;
		std::cerr << "Output boundary image:  " << parameters.outputBoundaryImageFilename << std::endl;
		std::cerr << "Output disparity image: " << parameters.outputDisparityImageFilename << std::endl;
		std::cerr << "Output flow image:      " << parameters.outputFlowImageFilename << std::endl;
		std::cerr << "   disparity factor:     " << parameters.disparityFactor << std::endl;
		std::cerr << "   #superpixels:         " << parameters.superpixelTotal << std::endl;
		std::cerr << std::endl;
	}

	try {
		// Open input image
		rev::Image<unsigned char> firstLeftImage = rev::readImageFile(parameters.firstLeftImageFilename);
		//by Harry
		std::string extension=getFilenameExtension(parameters.firstLeftDisparityImageFilename);
		rev::Image<unsigned short> firstLeftDisparityImage;
		if(extension == "pgm")
		{
            firstLeftDisparityImage=rev::readUShortPGM(parameters.firstLeftDisparityImageFilename);
		}
		else
		{
            firstLeftDisparityImage=rev::read16bitImageFile(parameters.firstLeftDisparityImageFilename);
		}
		CameraMotion cameraMotion;
		cameraMotion.readCameraMotionFile(parameters.cameraMotionFilename);

		clock_t startClock, endClock;
		startClock = clock();

		// StereoSLIC
		Smoothfit smoothfit;
		// Perform superpixel segmentation
		rev::Image<unsigned short> segmentImage;
		rev::Image<unsigned short> segmentDisparityImage;
		rev::Image<unsigned short> segmentFlowImage;
		rev::Image<unsigned char> boundaryLabelImage;
		std::vector< std::vector<int> > boundaryLabelList;
        // by harry, segmented plane list
        std::vector< std::vector<double> > disparityPlanes;

		smoothfit.segment(parameters.superpixelTotal,
			firstLeftImage, firstLeftDisparityImage,
			cameraMotion, 256,
			segmentImage, segmentDisparityImage, segmentFlowImage,
			boundaryLabelImage, boundaryLabelList,
			disparityPlanes);
		endClock = clock();
		if (parameters.verbose) {
			std::cerr << "Computation time: " << static_cast<double>(endClock - startClock)/CLOCKS_PER_SEC << " sec." << std::endl;
			std::cerr << std::endl;
		}

		// Output
		rev::write16bitImageFile(parameters.outputSegmentImageFilename, segmentImage);
		rev::write16bitImageFile(parameters.outputDisparityImageFilename, segmentDisparityImage);
		rev::write16bitImageFile(parameters.outputFlowImageFilename, segmentFlowImage);
		rev::Image<unsigned char> boundaryImage = rev::drawSegmentBoundary(firstLeftImage, segmentImage);
		rev::writeImageFile(parameters.outputBoundaryImageFilename, boundaryImage);
		rev::writeImageFile(parameters.outputBoundaryLabelImageFilename, boundaryLabelImage);
		writeBoundaryLabelFile(parameters.outputBoundaryLabelFilename, boundaryLabelList);
		//by harry
		writeDisparityPlaneFile(parameters.outputDisparityPlaneFilename,disparityPlanes, cameraMotion,
		parameters.fx, parameters.fy, parameters.ox, parameters.oy, parameters.B);

	} catch (const rev::Exception& revException) {
		std::cerr << "Error [" << revException.functionName() << "]: " << revException.message() << std::endl;
		exit(1);
	}
}
