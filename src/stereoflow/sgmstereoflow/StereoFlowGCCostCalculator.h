#pragma once

#include <revlib.h>
#include "CameraMotion.h"
#include "CalibratedEpipolarFlowGeometry.h"

class StereoFlowGCCostCalculator {
public:
	StereoFlowGCCostCalculator();
	StereoFlowGCCostCalculator(const int disparityTotal,
		const int costType,
		const int sobelCapValue,
		const int censusWindowHorizontalRadius,
		const int censusWindowVerticalRadius,
		const double censusWeightFactor,
		const int aggregationWindowRadius);

	void setParameter(const int disparityTotal,
		const int costType,
		const int sobelCapValue,
		const int censusWindowHorizontalRadius,
		const int censusWindowVerticalRadius,
		const double censusWeightFactor,
		const int aggregationWindowRadius);

	void computeCostImage(const rev::Image<unsigned char>& firstLeftImage,
		const rev::Image<unsigned char>& firstRightImage,
		const rev::Image<unsigned char>& secondLeftImage,
		const rev::Image<unsigned short>& stereoDisparityImage,
		const rev::Image<unsigned short>& flowVzIndexImage,
		const CameraMotion& cameraMotion,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		rev::Image<unsigned short>& leftCostImage,
		rev::Image<unsigned short>& rightCostImage);


private:
	void checkInputImages(const rev::Image<unsigned char>& leftImage,
		const rev::Image<unsigned char>& rightImage) const;
	void computeLeftCostImage(const rev::Image<unsigned char>& firstLeftImage,
		const rev::Image<unsigned char>& firstRightImage,
		const rev::Image<unsigned char>& secondLeftImage,
		const rev::Image<unsigned short>& stereoDisparityImage,
		const rev::Image<unsigned short>& flowVzIndexImage,
		const CameraMotion& cameraMotion,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		const bool sfflag,
		rev::Image<unsigned short>& leftCostImage);
	void makeRightCostImage(const rev::Image<unsigned short>& leftCostImage,
		const rev::Image<unsigned short>& stereoDisparityImage,
		rev::Image<unsigned short>& rightCostImage) const;

	void allocateDataBuffer();
	void makeFlowInsideFlagImage(const CameraMotion& cameraMotion, const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry);
	void calcTopRowCost(unsigned char*& leftSobelRow, unsigned char*& rightSobelRow, const int widthStepSobel,
		int*& leftCensusRow, int*& rightCensusRow, const int widthStepCensus,
		const rev::Image<short>& firstHorizontalSobelImage,
		const rev::Image<short>& firstVerticalSobelImage,
		const rev::Image<short>& secondHorizontalSobelImage,
		const rev::Image<short>& secondVerticalSobelImage,
		const unsigned char* capTable,
		const rev::Image<int>& firstCensusImage,
		const rev::Image<int>& secondCensusImage,
		const rev::Image<unsigned short>& stereoDisparityImage,
		const rev::Image<unsigned short>& flowVzIndexImage,
		const CameraMotion& cameraMotion,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		const bool sfflag,
		unsigned short* costImageRow);
	void calcRowCosts(unsigned char*& leftSobelRow, unsigned char*& rightSobelRow, const int widthStepSobel,
		int*& leftCensusRow, int*& rightCensusRow, const int widthStepCensus,
		const rev::Image<short>& firstHorizontalSobelImage,
		const rev::Image<short>& firstVerticalSobelImage,
		const rev::Image<short>& secondHorizontalSobelImage,
		const rev::Image<short>& secondVerticalSobelImage,
		const unsigned char* capTable,
		const rev::Image<int>& firstCensusImage,
		const rev::Image<int>& secondCensusImage,
		const rev::Image<unsigned short>& stereoDisparityImage,
		const rev::Image<unsigned short>& flowVzIndexImage,
		const CameraMotion& cameraMotion,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		const bool sfflag,
		unsigned short* costImageRow, const int widthStepCost);
	void calcPixelwiseSAD(const unsigned char* leftSobelRow, const unsigned char* rightSobelRow);
	void calcPixelwiseHamming(const int* leftCensusRow, const int* rightCensusRow, const bool addCost);
	void calcHalfPixelRight(const unsigned char* rightSobelRow);
	void addFlowPixelwiseCost(const rev::Image<short>& firstHorizontalSobelImage,
		const rev::Image<short>& firstVerticalSobelImage,
		const rev::Image<short>& secondHorizontalSobelImage,
		const rev::Image<short>& secondVerticalSobelImage,
		const unsigned char* capTable,
		const rev::Image<int>& firstCensusImage,
		const rev::Image<int>& secondCensusImage,
		const rev::Image<unsigned short>& stereoDisparityImage,
		const rev::Image<unsigned short>& flowVzIndexImage,
		const int y,
		const CameraMotion& cameraMotion,
		const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry);
	double directionalSobel(const rev::Image<short>& horizontalSobelImage,
		const rev::Image<short>& verticalSobelImage,
		const double epipolarDirectionX,
		const double epipolarDirectionY,
		const int x, const int y) const;
	int subpixelInterpolatedCappedSobel(const rev::Image<short>& horizontalSobelImage,
		const rev::Image<short>& verticalSobelImage,
		const unsigned char* capTable,
		const double epipolarDirectionX,
		const double epipolarDirectionY,
		const double x, const double y) const;

	// Parameters
	int costType_;
	int disparityTotal_;
	int sobelCapValue_;
	int censusWindowRadius_;
	double censusWeightFactor_;
	int aggregationWindowRadius_;

	// Image size
	int width_;
	int height_;
	// Data buffer
	rev::AlignedMemory<unsigned char> pixelwiseCostRow_;
	rev::AlignedMemory<unsigned short> rowAggregatedCost_;
	rev::AlignedMemory<unsigned char> halfPixelRightMin_;
	rev::AlignedMemory<unsigned char> halfPixelRightMax_;
	rev::Image<unsigned char> flowInsideFlagImage_;
};
