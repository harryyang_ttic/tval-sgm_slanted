#include "StereoFlowGCCostCalculator.h"
#include <emmintrin.h>
#include <nmmintrin.h>
#include "CappedSobelFilter.h"
#include "SobelFilter.h"
#include "CensusTransform.h"

const int STEREOFLOWGCCOSTCALCULATOR_DEFAULT_COST_TYPE = 2;   // 0:Gradient, 1:Census, 2:Gradient+Census
const int STEREOFLOWGCCOSTCALCULATOR_DEFAULT_DISPARITY_TOTAL = 256;
const int STEREOFLOWGCCOSTCALCULATOR_DEFAULT_SOBEL_CAP_VALUE = 15;
const int STEREOFLOWGCCOSTCALCULATOR_DEFAULT_CENSUS_WINDOW_RADIUS = 2;
const double STEREOFLOWGCCOSTCALCULATOR_DEFAULT_CENSUS_WEIGHT_FACTOR = 1.0/3.0;//1.0/6.0;
const int STEREOFLOWGCCOSTCALCULATOR_DEFAULT_AGGREGATION_WINDOW_RADIUS = 2;

StereoFlowGCCostCalculator::StereoFlowGCCostCalculator() : costType_(STEREOFLOWGCCOSTCALCULATOR_DEFAULT_COST_TYPE),
    disparityTotal_(STEREOFLOWGCCOSTCALCULATOR_DEFAULT_DISPARITY_TOTAL),
    sobelCapValue_(STEREOFLOWGCCOSTCALCULATOR_DEFAULT_SOBEL_CAP_VALUE),
    censusWindowRadius_(STEREOFLOWGCCOSTCALCULATOR_DEFAULT_CENSUS_WINDOW_RADIUS),
    censusWeightFactor_(STEREOFLOWGCCOSTCALCULATOR_DEFAULT_CENSUS_WEIGHT_FACTOR),
    aggregationWindowRadius_(STEREOFLOWGCCOSTCALCULATOR_DEFAULT_AGGREGATION_WINDOW_RADIUS),
    width_(0), height_(0) {}

StereoFlowGCCostCalculator::StereoFlowGCCostCalculator(const int disparityTotal,
        const int costType,
        const int sobelCapValue,
        const int censusWindowHorizontalRadius,
        const int censusWindowVerticalRadius,
        const double censusWeightFactor,
        const int aggregationWindowRadius)
{
    setParameter(disparityTotal, costType, sobelCapValue, censusWindowHorizontalRadius, censusWindowVerticalRadius, censusWeightFactor, aggregationWindowRadius);
}

void StereoFlowGCCostCalculator::setParameter(const int disparityTotal,
        const int costType,
        const int sobelCapValue,
        const int censusWindowHorizontalRadius,
        const int censusWindowVerticalRadius,
        const double censusWeightFactor,
        const int aggregationWindowRadius)
{
    // The number of disparities
    if (disparityTotal <= 0 || disparityTotal%16 != 0)
    {
        throw rev::Exception("StereoFlowGCCostCalculator::setParameters", "the number of disparities must be a multiple of 16");
    }
    disparityTotal_ = disparityTotal;

    // Cost type
    if (costType == 0) costType_ = 0;
    else if (costType == 1) costType_ = 1;
    else costType_ = 2;

    // Cap value of sobel filter
    sobelCapValue_ = std::max(sobelCapValue, 15);
    sobelCapValue_ = std::min(sobelCapValue_, 127) | 1;

    // Window size of Census transform
    censusWindowRadius_ = censusWindowHorizontalRadius;
    // Division factor of hamming distance between Census codes
    if (censusWeightFactor < 0)
    {
        throw rev::Exception("StereoFlowGCCostCalculator::setParameters", "weight of census transform must be positive");
    }
    censusWeightFactor_ = censusWeightFactor;

    // Window radius for aggregating pixelwise costs
    if (aggregationWindowRadius < 0)
    {
        throw rev::Exception("StereoFlowGCCostCalculator::setParameters", "window size of cost aggregation is less than zero");
    }
    aggregationWindowRadius_ = aggregationWindowRadius;
}

void StereoFlowGCCostCalculator::computeCostImage(const rev::Image<unsigned char>& firstLeftImage,
        const rev::Image<unsigned char>& firstRightImage,
        const rev::Image<unsigned char>& secondLeftImage,
        const rev::Image<unsigned short>& stereoDisparityImage,
        const rev::Image<unsigned short>& flowVzIndexImage,
        const CameraMotion& cameraMotion,
        const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
        rev::Image<unsigned short>& leftCostImage,
        rev::Image<unsigned short>& rightCostImage)
{
    computeLeftCostImage(firstLeftImage, firstRightImage, secondLeftImage, stereoDisparityImage, flowVzIndexImage, cameraMotion, epipolarFlowGeometry, false, leftCostImage);
    makeRightCostImage(leftCostImage, stereoDisparityImage, rightCostImage);
    computeLeftCostImage(firstLeftImage, firstRightImage, secondLeftImage, stereoDisparityImage, flowVzIndexImage, cameraMotion, epipolarFlowGeometry, true, leftCostImage);
}


void StereoFlowGCCostCalculator::checkInputImages(const rev::Image<unsigned char>& leftImage,
        const rev::Image<unsigned char>& rightImage) const
{
    if (leftImage.width() != rightImage.width() || leftImage.height() != rightImage.height())
    {
        throw rev::Exception("StereoFlowGCCostCalculator::checkInputImages", "sizes of input stereo images are not the same");
    }
}

void StereoFlowGCCostCalculator::computeLeftCostImage(const rev::Image<unsigned char>& firstLeftImage,
        const rev::Image<unsigned char>& firstRightImage,
        const rev::Image<unsigned char>& secondLeftImage,
        const rev::Image<unsigned short>& stereoDisparityImage,
        const rev::Image<unsigned short>& flowVzIndexImage,
        const CameraMotion& cameraMotion,
        const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
        const bool sfflag,
        rev::Image<unsigned short>& leftCostImage)
{
    // Image size
    width_ = firstLeftImage.width();
    height_ = firstLeftImage.height();
    // Convert to grayscale
    rev::Image<unsigned char> leftGrayscaleImage = rev::convertToGrayscale(firstLeftImage);
    rev::Image<unsigned char> rightGrayscaleImage = rev::convertToGrayscale(firstRightImage);
    rev::Image<unsigned char> leftGrayscaleImage2 = rev::convertToGrayscale(secondLeftImage);

    // Sobel filter
    CappedSobelFilter cappedSobel;
    cappedSobel.setCapValue(sobelCapValue_);
    rev::Image<unsigned char> leftCappedSobelImage;
    cappedSobel.filter(leftGrayscaleImage, leftCappedSobelImage);
    rev::Image<unsigned char> flippedRightCappedSobelImage;
    cappedSobel.filter(rightGrayscaleImage, flippedRightCappedSobelImage, true);

    SobelFilter sobel;
    rev::Image<short> firstHorizontalSobelImage, firstVerticalSobelImage;
    sobel.filterVerticalHorizontal(leftGrayscaleImage, firstHorizontalSobelImage, firstVerticalSobelImage);
    rev::Image<short> secondHorizontalSobelImage, secondVerticalSobelImage;
    sobel.filterVerticalHorizontal(leftGrayscaleImage2, secondHorizontalSobelImage, secondVerticalSobelImage);

    // Census transform
    CensusTransform census;
    census.setWindowRadius(censusWindowRadius_);
    rev::Image<int> firstLeftCensusCodeImage;
    rev::Image<int> firstRightCensusCodeImage;
    rev::Image<int> secondLeftCensusCodeImage;
    census.transform(leftGrayscaleImage, firstLeftCensusCodeImage);
    census.transform(rightGrayscaleImage, firstRightCensusCodeImage);
    census.transform(leftGrayscaleImage2, secondLeftCensusCodeImage);

    // Compute cost image
    allocateDataBuffer();
    // Look up table for cap of sobel value
    const int tableOffset = 256*4;
    const int tableSize = tableOffset*2 + 256;
    unsigned char convertTable[tableSize];
    for (int i = 0; i < tableSize; ++i)
    {
        convertTable[i] = static_cast<unsigned char>(std::min(std::max(i - tableOffset, -sobelCapValue_), sobelCapValue_) + sobelCapValue_);
    }
    unsigned char* capTable = convertTable + tableOffset;
    makeFlowInsideFlagImage(cameraMotion, epipolarFlowGeometry);

    leftCostImage.resize(width_, height_, disparityTotal_);
    leftCostImage.setTo(0);

    unsigned char* leftCappedSobelRow = leftCappedSobelImage.dataPointer();
    unsigned char* rightCappedSobelRow = flippedRightCappedSobelImage.dataPointer();
    int widthStepSobel = leftCappedSobelImage.widthStep();
    int* leftCensusRow = firstLeftCensusCodeImage.dataPointer();
    int* rightCensusRow = firstRightCensusCodeImage.dataPointer();
    int widthStepCensus = firstLeftCensusCodeImage.widthStep();
    unsigned short* costImageRow = leftCostImage.dataPointer();
    int widthStepCost = leftCostImage.widthStep();

    calcTopRowCost(leftCappedSobelRow, rightCappedSobelRow, widthStepSobel,
                   leftCensusRow, rightCensusRow, widthStepCensus,
                   firstHorizontalSobelImage, firstVerticalSobelImage, secondHorizontalSobelImage, secondVerticalSobelImage,
                   capTable, firstLeftCensusCodeImage, secondLeftCensusCodeImage, stereoDisparityImage, flowVzIndexImage, cameraMotion, epipolarFlowGeometry, sfflag,
                   costImageRow);
    costImageRow += widthStepCost;
    calcRowCosts(leftCappedSobelRow, rightCappedSobelRow, widthStepSobel,
                 leftCensusRow, rightCensusRow, widthStepCensus,
                 firstHorizontalSobelImage, firstVerticalSobelImage, secondHorizontalSobelImage, secondVerticalSobelImage,
                 capTable, firstLeftCensusCodeImage, secondLeftCensusCodeImage, stereoDisparityImage, flowVzIndexImage, cameraMotion, epipolarFlowGeometry, sfflag,
                 costImageRow, widthStepCost);
}

void StereoFlowGCCostCalculator::makeRightCostImage(const rev::Image<unsigned short>& leftCostImage,
        const rev::Image<unsigned short>& stereoDisparityImage,
        rev::Image<unsigned short>& rightCostImage) const
{
    rightCostImage.resize(width_, height_, disparityTotal_);

    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            int maxDisparityIndex = std::min(disparityTotal_, width_ - x);
            for (int d = 0; d < maxDisparityIndex; ++d)
            {
                rightCostImage(x, y, d) = leftCostImage(x + d, y, d);
            }
            unsigned short lastValue = rightCostImage(x, y, maxDisparityIndex - 1);
            for (int d = maxDisparityIndex; d < disparityTotal_; ++d)
            {
                rightCostImage(x, y, d) = lastValue;
            }
        }
    }
}


void StereoFlowGCCostCalculator::allocateDataBuffer()
{
    int pixelwiseCostRowBufferSize = width_*disparityTotal_;
    int rowAggregatedCostBufferSize = width_*disparityTotal_*(aggregationWindowRadius_*2 + 2);
    int halfPixelRightBufferSize = width_;

    pixelwiseCostRow_.resize(pixelwiseCostRowBufferSize);
    rowAggregatedCost_.resize(rowAggregatedCostBufferSize);
    halfPixelRightMin_.resize(halfPixelRightBufferSize);
    halfPixelRightMax_.resize(halfPixelRightBufferSize);
}

void StereoFlowGCCostCalculator::makeFlowInsideFlagImage(const CameraMotion& cameraMotion, const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry)
{
    flowInsideFlagImage_.resize(width_, height_, disparityTotal_);
    for (int y = 0; y < height_; ++y)
    {
        for (int x = 0; x < width_; ++x)
        {
            double distanceFromEpipole = epipolarFlowGeometry.distanceFromEpipole(x, y);
            double noDisparityPointX = epipolarFlowGeometry.noDisparityPointX(x, y);
            double noDisparityPointY = epipolarFlowGeometry.noDisparityPointY(x, y);
            double epipolarDirectionX = epipolarFlowGeometry.epipolarDirectionX(x, y);
            double epipolarDirectionY = epipolarFlowGeometry.epipolarDirectionY(x, y);

            for (int d = 0; d < disparityTotal_; ++d)
            {
                double vzRatio = cameraMotion.ratioVzDisparity(x, y)*d;
                if (1.0 - vzRatio < 1e-6)
                {
                    flowInsideFlagImage_(x, y, d) = 0;
                    continue;
                }
                double disparity = distanceFromEpipole*vzRatio/(1 - vzRatio);
                double secondX = noDisparityPointX + epipolarDirectionX*disparity;
                double secondY = noDisparityPointY + epipolarDirectionY*disparity;
                int secondXInt = static_cast<int>(secondX + 0.5);
                int secondYInt = static_cast<int>(secondY + 0.5);

                if (secondX < 0 || secondXInt >= width_ || secondY < 0 || secondYInt >= height_) flowInsideFlagImage_(x, y, d) = 0;
                else flowInsideFlagImage_(x, y, d) = 1;
            }
        }
    }
}

void StereoFlowGCCostCalculator::calcTopRowCost(unsigned char*& leftSobelRow, unsigned char*& rightSobelRow, const int widthStepSobel,
        int*& leftCensusRow, int*& rightCensusRow, const int widthStepCensus,
        const rev::Image<short>& firstHorizontalSobelImage,
        const rev::Image<short>& firstVerticalSobelImage,
        const rev::Image<short>& secondHorizontalSobelImage,
        const rev::Image<short>& secondVerticalSobelImage,
        const unsigned char* capTable,
        const rev::Image<int>& firstCensusImage,
        const rev::Image<int>& secondCensusImage,
        const rev::Image<unsigned short>& stereoDisparityImage,
        const rev::Image<unsigned short>& flowVzIndexImage,
        const CameraMotion& cameraMotion,
        const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
        const bool sfflag,
        unsigned short* costImageRow)
{
    for (int rowIndex = 0; rowIndex <= aggregationWindowRadius_; ++rowIndex)
    {
        int rowAggregatedCostIndex = std::min(rowIndex, height_ - 1)%(aggregationWindowRadius_*2 + 2);
        unsigned short* rowAggregatedCostCurrent = rowAggregatedCost_.pointer() + rowAggregatedCostIndex*width_*disparityTotal_;

        if (costType_ == 0)
        {
            calcPixelwiseSAD(leftSobelRow, rightSobelRow);
        }
        else if (costType_ == 1)
        {
            calcPixelwiseHamming(leftCensusRow, rightCensusRow, false);
        }
        else
        {
            calcPixelwiseSAD(leftSobelRow, rightSobelRow);
            calcPixelwiseHamming(leftCensusRow, rightCensusRow, true);

            if (sfflag)
            {
                addFlowPixelwiseCost(firstHorizontalSobelImage, firstVerticalSobelImage, secondHorizontalSobelImage, secondVerticalSobelImage,
                                     capTable, firstCensusImage, secondCensusImage, stereoDisparityImage, flowVzIndexImage, rowIndex, cameraMotion, epipolarFlowGeometry);
            }
            else
            {
                for (int x = 0; x < width_; ++x)
                {
                    for (int d = 0; d < disparityTotal_; ++d) pixelwiseCostRow_[disparityTotal_*x + d] *= 2;
                }
            }
        }

        memset(rowAggregatedCostCurrent, 0, disparityTotal_*sizeof(unsigned short));
        // x = 0
        for (int x = 0; x <= aggregationWindowRadius_; ++x)
        {
            int scale = x == 0 ? aggregationWindowRadius_ + 1 : 1;
            for (int d = 0; d < disparityTotal_; ++d)
            {
                rowAggregatedCostCurrent[d] += static_cast<unsigned short>(pixelwiseCostRow_[disparityTotal_*x + d]*scale);
            }
        }
        // x = 1...width-1
        for (int x = 1; x < width_; ++x)
        {
            const unsigned char* addPixelwiseCost = pixelwiseCostRow_.pointer()
                                                    + std::min((x + aggregationWindowRadius_)*disparityTotal_, (width_ - 1)*disparityTotal_);
            const unsigned char* subPixelwiseCost = pixelwiseCostRow_.pointer()
                                                    + std::max((x - aggregationWindowRadius_ - 1)*disparityTotal_, 0);

            for (int d = 0; d < disparityTotal_; ++d)
            {
                rowAggregatedCostCurrent[disparityTotal_*x + d]
                    = static_cast<unsigned short>(rowAggregatedCostCurrent[disparityTotal_*(x - 1) + d]
                                                  + addPixelwiseCost[d] - subPixelwiseCost[d]);
            }
        }

        // Add to cost
        int scale = rowIndex == 0 ? aggregationWindowRadius_ + 1 : 1;
        for (int i = 0; i < width_*disparityTotal_; ++i)
        {
            costImageRow[i] += rowAggregatedCostCurrent[i]*scale;
        }

        leftSobelRow += widthStepSobel;
        rightSobelRow += widthStepSobel;
        leftCensusRow += widthStepCensus;
        rightCensusRow += widthStepCensus;
    }
}

void StereoFlowGCCostCalculator::calcRowCosts(unsigned char*& leftSobelRow, unsigned char*& rightSobelRow, const int widthStepSobel,
        int*& leftCensusRow, int*& rightCensusRow, const int widthStepCensus,
        const rev::Image<short>& firstHorizontalSobelImage,
        const rev::Image<short>& firstVerticalSobelImage,
        const rev::Image<short>& secondHorizontalSobelImage,
        const rev::Image<short>& secondVerticalSobelImage,
        const unsigned char* capTable,
        const rev::Image<int>& firstCensusImage,
        const rev::Image<int>& secondCensusImage,
        const rev::Image<unsigned short>& stereoDisparityImage,
        const rev::Image<unsigned short>& flowVzIndexImage,
        const CameraMotion& cameraMotion,
        const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
        const bool sfflag,
        unsigned short* costImageRow, const int widthStepCost)
{
    const __m128i registerZero = _mm_setzero_si128();

    for (int y = 1; y < height_; ++y)
    {
        int addRowIndex = y + aggregationWindowRadius_;
        int addRowAggregatedCostIndex = std::min(addRowIndex, height_ - 1)%(aggregationWindowRadius_*2 + 2);
        unsigned short* addRowAggregatedCost = rowAggregatedCost_.pointer() + width_*disparityTotal_*addRowAggregatedCostIndex;

        if (addRowIndex < height_)
        {
            if (costType_ == 0)
            {
                calcPixelwiseSAD(leftSobelRow, rightSobelRow);
            }
            else if (costType_ == 1)
            {
                calcPixelwiseHamming(leftCensusRow, rightCensusRow, false);
            }
            else
            {
                calcPixelwiseSAD(leftSobelRow, rightSobelRow);
                calcPixelwiseHamming(leftCensusRow, rightCensusRow, true);
                //for (int x = 0; x < width_; ++x) {
                //	if (stereoDisparityImage(x, addRowIndex) == 0) {
                //		for (int d = 0; d < disparityTotal_; ++d) pixelwiseCostRow_[disparityTotal_*x + d] = 0;
                //	}
                //}
                if (sfflag)
                {
                    addFlowPixelwiseCost(firstHorizontalSobelImage, firstVerticalSobelImage, secondHorizontalSobelImage, secondVerticalSobelImage,
                                         capTable, firstCensusImage, secondCensusImage, stereoDisparityImage, flowVzIndexImage, addRowIndex, cameraMotion, epipolarFlowGeometry);
                }
                else
                {
                    for (int x = 0; x < width_; ++x)
                    {
                        for (int d = 0; d < disparityTotal_; ++d) pixelwiseCostRow_[disparityTotal_*x + d] *= 2;
                    }
                }
            }

            memset(addRowAggregatedCost, 0, disparityTotal_*sizeof(unsigned short));
            // x = 0
            for (int x = 0; x <= aggregationWindowRadius_; ++x)
            {
                int scale = x == 0 ? aggregationWindowRadius_ + 1 : 1;
                for (int d = 0; d < disparityTotal_; ++d)
                {
                    addRowAggregatedCost[d] += static_cast<unsigned short>(pixelwiseCostRow_[disparityTotal_*x + d]*scale);
                }
            }
            // x = 1...width-1
            int subRowAggregatedCostIndex = std::max(y - aggregationWindowRadius_ - 1, 0)%(aggregationWindowRadius_*2 + 2);
            const unsigned short* subRowAggregatedCost = rowAggregatedCost_.pointer() + width_*disparityTotal_*subRowAggregatedCostIndex;
            const unsigned short* previousCostRow = costImageRow - widthStepCost;
            for (int x = 1; x < width_; ++x)
            {
                const unsigned char* addPixelwiseCost = pixelwiseCostRow_.pointer()
                                                        + std::min((x + aggregationWindowRadius_)*disparityTotal_, (width_ - 1)*disparityTotal_);
                const unsigned char* subPixelwiseCost = pixelwiseCostRow_.pointer()
                                                        + std::max((x - aggregationWindowRadius_ - 1)*disparityTotal_, 0);

                for (int d = 0; d < disparityTotal_; d += 16)
                {
                    __m128i registerAddPixelwiseLow = _mm_load_si128(reinterpret_cast<const __m128i*>(addPixelwiseCost + d));
                    __m128i registerAddPixelwiseHigh = _mm_unpackhi_epi8(registerAddPixelwiseLow, registerZero);
                    registerAddPixelwiseLow = _mm_unpacklo_epi8(registerAddPixelwiseLow, registerZero);
                    __m128i registerSubPixelwiseLow = _mm_load_si128(reinterpret_cast<const __m128i*>(subPixelwiseCost + d));
                    __m128i registerSubPixelwiseHigh = _mm_unpackhi_epi8(registerSubPixelwiseLow, registerZero);
                    registerSubPixelwiseLow = _mm_unpacklo_epi8(registerSubPixelwiseLow, registerZero);

                    // Low
                    __m128i registerAddAggregated = _mm_load_si128(reinterpret_cast<const __m128i*>(addRowAggregatedCost
                                                    + disparityTotal_*(x - 1) + d));
                    registerAddAggregated = _mm_adds_epi16(_mm_subs_epi16(registerAddAggregated, registerSubPixelwiseLow),
                                                           registerAddPixelwiseLow);
                    __m128i registerCost = _mm_load_si128(reinterpret_cast<const __m128i*>(previousCostRow + disparityTotal_*x + d));
                    registerCost = _mm_adds_epi16(_mm_subs_epi16(registerCost,
                                                  _mm_load_si128(reinterpret_cast<const __m128i*>(subRowAggregatedCost + disparityTotal_*x + d))),
                                                  registerAddAggregated);
                    _mm_store_si128(reinterpret_cast<__m128i*>(addRowAggregatedCost + disparityTotal_*x + d), registerAddAggregated);
                    _mm_store_si128(reinterpret_cast<__m128i*>(costImageRow + disparityTotal_*x + d), registerCost);

                    // High
                    registerAddAggregated = _mm_load_si128(reinterpret_cast<const __m128i*>(addRowAggregatedCost + disparityTotal_*(x-1) + d + 8));
                    registerAddAggregated = _mm_adds_epi16(_mm_subs_epi16(registerAddAggregated, registerSubPixelwiseHigh),
                                                           registerAddPixelwiseHigh);
                    registerCost = _mm_load_si128(reinterpret_cast<const __m128i*>(previousCostRow + disparityTotal_*x + d + 8));
                    registerCost = _mm_adds_epi16(_mm_subs_epi16(registerCost,
                                                  _mm_load_si128(reinterpret_cast<const __m128i*>(subRowAggregatedCost + disparityTotal_*x + d + 8))),
                                                  registerAddAggregated);
                    _mm_store_si128(reinterpret_cast<__m128i*>(addRowAggregatedCost + disparityTotal_*x + d + 8), registerAddAggregated);
                    _mm_store_si128(reinterpret_cast<__m128i*>(costImageRow + disparityTotal_*x + d + 8), registerCost);
                }
            }
        }

        leftSobelRow += widthStepSobel;
        rightSobelRow += widthStepSobel;
        leftCensusRow += widthStepCensus;
        rightCensusRow += widthStepCensus;
        costImageRow += widthStepCost;
    }
}

void StereoFlowGCCostCalculator::calcPixelwiseSAD(const unsigned char* leftSobelRow, const unsigned char* rightSobelRow)
{
    calcHalfPixelRight(rightSobelRow);

    for (int x = 0; x < 16; ++x)
    {
        int leftCenterValue = leftSobelRow[x];
        int leftHalfLeftValue = x > 0 ? (leftCenterValue + leftSobelRow[x - 1])/2 : leftCenterValue;
        int leftHalfRightValue = x < width_ - 1 ? (leftCenterValue + leftSobelRow[x + 1])/2 : leftCenterValue;
        int leftMinValue = std::min(leftHalfLeftValue, leftHalfRightValue);
        leftMinValue = std::min(leftMinValue, leftCenterValue);
        int leftMaxValue = std::max(leftHalfLeftValue, leftHalfRightValue);
        leftMaxValue = std::max(leftMaxValue, leftCenterValue);

        for (int d = 0; d <= x; ++d)
        {
            int rightCenterValue = rightSobelRow[width_ - 1 - x + d];
            int rightMinValue = halfPixelRightMin_[width_ - 1 - x + d];
            int rightMaxValue = halfPixelRightMax_[width_ - 1 - x + d];

            int costLtoR = std::max(0, leftCenterValue - rightMaxValue);
            costLtoR = std::max(costLtoR, rightMinValue - leftCenterValue);
            int costRtoL = std::max(0, rightCenterValue - leftMaxValue);
            costRtoL = std::max(costRtoL, leftMinValue - rightCenterValue);
            int costValue = std::min(costLtoR, costRtoL);

            pixelwiseCostRow_[disparityTotal_*x + d] = costValue;
        }
        for (int d = x + 1; d < disparityTotal_; ++d)
        {
            pixelwiseCostRow_[disparityTotal_*x + d] = pixelwiseCostRow_[disparityTotal_*x + d - 1];
        }
    }
    for (int x = 16; x < disparityTotal_; ++x)
    {
        int leftCenterValue = leftSobelRow[x];
        int leftHalfLeftValue = x > 0 ? (leftCenterValue + leftSobelRow[x - 1])/2 : leftCenterValue;
        int leftHalfRightValue = x < width_ - 1 ? (leftCenterValue + leftSobelRow[x + 1])/2 : leftCenterValue;
        int leftMinValue = std::min(leftHalfLeftValue, leftHalfRightValue);
        leftMinValue = std::min(leftMinValue, leftCenterValue);
        int leftMaxValue = std::max(leftHalfLeftValue, leftHalfRightValue);
        leftMaxValue = std::max(leftMaxValue, leftCenterValue);

        __m128i registerLeftCenterValue = _mm_set1_epi8(static_cast<char>(leftCenterValue));
        __m128i registerLeftMinValue =_mm_set1_epi8(static_cast<char>(leftMinValue));
        __m128i registerLeftMaxValue =_mm_set1_epi8(static_cast<char>(leftMaxValue));

        for (int d = 0; d < x/16; d += 16)
        {
            __m128i registerRightCenterValue = _mm_loadu_si128(reinterpret_cast<const __m128i*>(rightSobelRow + width_ - 1 - x + d));
            __m128i registerRightMinValue = _mm_loadu_si128(reinterpret_cast<const __m128i*>(halfPixelRightMin_.pointer() + width_ - 1 - x + d));
            __m128i registerRightMaxValue = _mm_loadu_si128(reinterpret_cast<const __m128i*>(halfPixelRightMax_.pointer() + width_ - 1 - x + d));

            __m128i registerCostLtoR = _mm_max_epu8(_mm_subs_epu8(registerLeftCenterValue, registerRightMaxValue),
                                                    _mm_subs_epu8(registerRightMinValue, registerLeftCenterValue));
            __m128i registerCostRtoL = _mm_max_epu8(_mm_subs_epu8(registerRightCenterValue, registerLeftMaxValue),
                                                    _mm_subs_epu8(registerLeftMinValue, registerRightCenterValue));
            __m128i registerCost = _mm_min_epu8(registerCostLtoR, registerCostRtoL);

            _mm_store_si128(reinterpret_cast<__m128i*>(pixelwiseCostRow_.pointer() + disparityTotal_*x + d), registerCost);
        }
        for (int d = x/16; d <= x; ++d)
        {
            int rightCenterValue = rightSobelRow[width_ - 1 - x + d];
            int rightMinValue = halfPixelRightMin_[width_ - 1 - x + d];
            int rightMaxValue = halfPixelRightMax_[width_ - 1 - x + d];

            int costLtoR = std::max(0, leftCenterValue - rightMaxValue);
            costLtoR = std::max(costLtoR, rightMinValue - leftCenterValue);
            int costRtoL = std::max(0, rightCenterValue - leftMaxValue);
            costRtoL = std::max(costRtoL, leftMinValue - rightCenterValue);
            int costValue = std::min(costLtoR, costRtoL);

            pixelwiseCostRow_[disparityTotal_*x + d] = costValue;
        }
        for (int d = x + 1; d < disparityTotal_; ++d)
        {
            pixelwiseCostRow_[disparityTotal_*x + d] = pixelwiseCostRow_[disparityTotal_*x + d - 1];
        }
    }
    for (int x = disparityTotal_; x < width_; ++x)
    {
        int leftCenterValue = leftSobelRow[x];
        int leftHalfLeftValue = x > 0 ? (leftCenterValue + leftSobelRow[x - 1])/2 : leftCenterValue;
        int leftHalfRightValue = x < width_ - 1 ? (leftCenterValue + leftSobelRow[x + 1])/2 : leftCenterValue;
        int leftMinValue = std::min(leftHalfLeftValue, leftHalfRightValue);
        leftMinValue = std::min(leftMinValue, leftCenterValue);
        int leftMaxValue = std::max(leftHalfLeftValue, leftHalfRightValue);
        leftMaxValue = std::max(leftMaxValue, leftCenterValue);

        __m128i registerLeftCenterValue = _mm_set1_epi8(static_cast<char>(leftCenterValue));
        __m128i registerLeftMinValue =_mm_set1_epi8(static_cast<char>(leftMinValue));
        __m128i registerLeftMaxValue =_mm_set1_epi8(static_cast<char>(leftMaxValue));

        for (int d = 0; d < disparityTotal_; d += 16)
        {
            __m128i registerRightCenterValue = _mm_loadu_si128(reinterpret_cast<const __m128i*>(rightSobelRow + width_ - 1 - x + d));
            __m128i registerRightMinValue = _mm_loadu_si128(reinterpret_cast<const __m128i*>(halfPixelRightMin_.pointer() + width_ - 1 - x + d));
            __m128i registerRightMaxValue = _mm_loadu_si128(reinterpret_cast<const __m128i*>(halfPixelRightMax_.pointer() + width_ - 1 - x + d));

            __m128i registerCostLtoR = _mm_max_epu8(_mm_subs_epu8(registerLeftCenterValue, registerRightMaxValue),
                                                    _mm_subs_epu8(registerRightMinValue, registerLeftCenterValue));
            __m128i registerCostRtoL = _mm_max_epu8(_mm_subs_epu8(registerRightCenterValue, registerLeftMaxValue),
                                                    _mm_subs_epu8(registerLeftMinValue, registerRightCenterValue));
            __m128i registerCost = _mm_min_epu8(registerCostLtoR, registerCostRtoL);

            _mm_store_si128(reinterpret_cast<__m128i*>(pixelwiseCostRow_.pointer() + disparityTotal_*x + d), registerCost);
        }
    }
}

void StereoFlowGCCostCalculator::calcPixelwiseHamming(const int* leftCensusRow, const int* rightCensusRow, const bool addCost)
{
    for (int x = 0; x < disparityTotal_; ++x)
    {
        int leftCencusCode = leftCensusRow[x];
        int hammingDistance = 0;
        for (int d = 0; d <= x; ++d)
        {
            int rightCensusCode = rightCensusRow[x - d];
            hammingDistance = static_cast<int>(_mm_popcnt_u32(static_cast<unsigned int>(leftCencusCode^rightCensusCode)));
            if (addCost) pixelwiseCostRow_[disparityTotal_*x + d] += static_cast<unsigned char>(hammingDistance*censusWeightFactor_);
            else pixelwiseCostRow_[disparityTotal_*x + d] = static_cast<unsigned char>(hammingDistance*censusWeightFactor_);
        }
        hammingDistance = static_cast<unsigned char>(hammingDistance*censusWeightFactor_);
        for (int d = x + 1; d < disparityTotal_; ++d)
        {
            if (addCost) pixelwiseCostRow_[disparityTotal_*x + d] += hammingDistance;
            else pixelwiseCostRow_[disparityTotal_*x + d] = hammingDistance;
        }
    }
    for (int x = disparityTotal_; x < width_; ++x)
    {
        int leftCencusCode = leftCensusRow[x];
        for (int d = 0; d < disparityTotal_; ++d)
        {
            int rightCensusCode = rightCensusRow[x - d];
            int hammingDistance = static_cast<int>(_mm_popcnt_u32(static_cast<unsigned int>(leftCencusCode^rightCensusCode)));
            if (addCost) pixelwiseCostRow_[disparityTotal_*x + d] += static_cast<unsigned char>(hammingDistance*censusWeightFactor_);
            else pixelwiseCostRow_[disparityTotal_*x + d] = static_cast<unsigned char>(hammingDistance*censusWeightFactor_);
        }
    }
}

void StereoFlowGCCostCalculator::calcHalfPixelRight(const unsigned char* rightSobelRow)
{
    for (int x = 0; x < width_; ++x)
    {
        int centerValue = rightSobelRow[x];
        int leftHalfValue = x > 0 ? (centerValue + rightSobelRow[x - 1])/2 : centerValue;
        int rightHalfValue = x < width_ - 1 ? (centerValue + rightSobelRow[x + 1])/2 : centerValue;
        int minValue = std::min(leftHalfValue, rightHalfValue);
        minValue = std::min(minValue, centerValue);
        int maxValue = std::max(leftHalfValue, rightHalfValue);
        maxValue = std::max(maxValue, centerValue);

        halfPixelRightMin_[x] = minValue;
        halfPixelRightMax_[x] = maxValue;
    }
}

void StereoFlowGCCostCalculator::addFlowPixelwiseCost(const rev::Image<short>& firstHorizontalSobelImage,
        const rev::Image<short>& firstVerticalSobelImage,
        const rev::Image<short>& secondHorizontalSobelImage,
        const rev::Image<short>& secondVerticalSobelImage,
        const unsigned char* capTable,
        const rev::Image<int>& firstCensusImage,
        const rev::Image<int>& secondCensusImage,
        const rev::Image<unsigned short>& stereoDisparityImage,
        const rev::Image<unsigned short>& flowVzIndexImage,
        const int y,
        const CameraMotion& cameraMotion,
        const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry)
{
    const double flowCostWeight = 1.0;
    const double flowCensusWeight = censusWeightFactor_;// 1.0/6.0;

    for (int x = 0; x < width_; ++x)
    {
        if (flowVzIndexImage(x, y) == 0)
        {
            for (int d = 0; d < disparityTotal_; ++d)
            {
                pixelwiseCostRow_[disparityTotal_*x + d] = static_cast<unsigned short>(pixelwiseCostRow_[disparityTotal_*x + d]*(1 + flowCostWeight));
            }
            continue;
        }

        double distanceFromEpipole = epipolarFlowGeometry.distanceFromEpipole(x, y);
        double noDisparityPointX = epipolarFlowGeometry.noDisparityPointX(x, y);
        double noDisparityPointY = epipolarFlowGeometry.noDisparityPointY(x, y);
        double epipolarDirectionX = epipolarFlowGeometry.epipolarDirectionX(x, y);
        double epipolarDirectionY = epipolarFlowGeometry.epipolarDirectionY(x, y);

        double firstOriginalSobelValue = directionalSobel(firstHorizontalSobelImage, firstVerticalSobelImage,
                                         epipolarDirectionX, epipolarDirectionY, x, y);
        int firstSobelValue = capTable[static_cast<int>(firstOriginalSobelValue + 0.5)];

        int firstCensusCode = firstCensusImage(x, y);

        for (int d = 0; d < disparityTotal_; ++d)
        {
            if (flowInsideFlagImage_(x, y, d) == 0)
            {
                pixelwiseCostRow_[disparityTotal_*x + d] = static_cast<unsigned short>(pixelwiseCostRow_[disparityTotal_*x + d]*(1 + flowCostWeight));
                continue;
            }

            double vzRatio = cameraMotion.ratioVzDisparity(x, y)*d;
            double disparity = distanceFromEpipole*vzRatio/(1 - vzRatio);
            double secondX = noDisparityPointX + epipolarDirectionX*disparity;
            double secondY = noDisparityPointY + epipolarDirectionY*disparity;
            int secondXInt = static_cast<int>(secondX + 0.5);
            int secondYInt = static_cast<int>(secondY + 0.5);

            if (secondX < 0 || secondXInt >= width_ || secondY < 0 || secondYInt >= height_)
            {
                pixelwiseCostRow_[disparityTotal_*x + d] = static_cast<unsigned short>(pixelwiseCostRow_[disparityTotal_*x + d]*(1 + flowCostWeight));
                continue;
            }

            double previousVzRatio = vzRatio;
            if (d > 0 && flowInsideFlagImage_(x, y, d - 1) == 1)
            {
                previousVzRatio = cameraMotion.ratioVzDisparity(x, y)*(d - 1);
            }
            double nextVzRatio = vzRatio;
            if (d < disparityTotal_ - 1 && flowInsideFlagImage_(x, y, d + 1) == 1)
            {
                nextVzRatio = cameraMotion.ratioVzDisparity(x, y)*(d + 1);
            }
            double previousDisparity = distanceFromEpipole*previousVzRatio/(1 - previousVzRatio);
            double nextDisparity = distanceFromEpipole*nextVzRatio/(1 - nextVzRatio);
            double previousHalfDisparity = (disparity + previousDisparity)/2.0 - disparity;
            if (previousHalfDisparity > -0.5) previousHalfDisparity = -0.5;
            double nextHalfDisparity = (disparity + nextDisparity)/2.0 - disparity;
            if (nextHalfDisparity < 0.5) nextHalfDisparity = 0.5;
            double previousHalfOffsetX = epipolarDirectionX*previousHalfDisparity;
            double previousHalfOffsetY = epipolarDirectionY*previousHalfDisparity;
            double nextHalfOffsetX = epipolarDirectionX*nextHalfDisparity;
            double nextHalfOffsetY = epipolarDirectionY*nextHalfDisparity;

            int firstPreviousHalfSobelValue = subpixelInterpolatedCappedSobel(firstHorizontalSobelImage, firstVerticalSobelImage,
                                              capTable, epipolarDirectionX, epipolarDirectionY,
                                              x + previousHalfOffsetX, y + previousHalfOffsetY);
            int firstNextHalfSobelValue = subpixelInterpolatedCappedSobel(firstHorizontalSobelImage, firstVerticalSobelImage,
                                          capTable, epipolarDirectionX, epipolarDirectionY,
                                          x + nextHalfOffsetX, y + nextHalfOffsetY);

            int firstMinSobelValue = std::min(firstSobelValue, firstPreviousHalfSobelValue);
            firstMinSobelValue = std::min(firstMinSobelValue, firstNextHalfSobelValue);
            int firstMaxSobelValue = std::max(firstSobelValue, firstPreviousHalfSobelValue);
            firstMaxSobelValue = std::max(firstMaxSobelValue, firstNextHalfSobelValue);

            //if (secondX < 0 || secondXInt >= width_ || secondY < 0 || secondYInt >= height_) {
            //	if (d > 0) {
            //		//pixelwiseCostRow_[disparityTotal_*x + d] += pixelwiseCostRow_[disparityTotal_*x + d - 1];
            //		pixelwiseCostRow_[disparityTotal_*x + d] = pixelwiseCostRow_[disparityTotal_*x + d - 1];
            //	} else {
            //		//pixelwiseCostRow_[disparityTotal_*x + d] += capTable[0] + 16;
            //		pixelwiseCostRow_[disparityTotal_*x + d] = capTable[0] + 16;
            //	}
            //	//pixelwiseCostRow_[disparityTotal_*x + d] = static_cast<unsigned short>(pixelwiseCostRow_[disparityTotal_*x + d]*(1 + flowCostWeight));
            //	continue;
            //}

            int secondSobelValue = subpixelInterpolatedCappedSobel(secondHorizontalSobelImage, secondVerticalSobelImage,
                                   capTable, epipolarDirectionX, epipolarDirectionY,
                                   secondX, secondY);
            int secondCensusCode = secondCensusImage(secondXInt, secondYInt);

            int secondPreviousHalfSobelValue = subpixelInterpolatedCappedSobel(secondHorizontalSobelImage, secondVerticalSobelImage,
                                               capTable, epipolarDirectionX, epipolarDirectionY,
                                               secondX + previousHalfOffsetX, secondY + previousHalfOffsetY);
            int secondNextHalfSobelValue = subpixelInterpolatedCappedSobel(secondHorizontalSobelImage, secondVerticalSobelImage,
                                           capTable, epipolarDirectionX, epipolarDirectionY,
                                           secondX + nextHalfOffsetX, secondY + nextHalfOffsetY);

            int secondMinSobelValue = std::min(secondSobelValue, secondPreviousHalfSobelValue);
            secondMinSobelValue = std::min(secondMinSobelValue, secondNextHalfSobelValue);
            int secondMaxSobelValue = std::max(secondSobelValue, secondPreviousHalfSobelValue);
            secondMaxSobelValue = std::max(secondMaxSobelValue, secondNextHalfSobelValue);

            int sobelCostFirstToSecond = std::max(0, firstSobelValue - secondMaxSobelValue);
            sobelCostFirstToSecond = std::max(sobelCostFirstToSecond, secondMinSobelValue - firstSobelValue);
            int sobelCostSecondToFirst = std::max(0, secondSobelValue - firstMaxSobelValue);
            sobelCostSecondToFirst = std::max(sobelCostSecondToFirst, firstMinSobelValue - secondSobelValue);
            int sobelCost = std::min(sobelCostFirstToSecond, sobelCostSecondToFirst);

            //if (x == 910 && y == 170) {
            //	//std::cout << d << ", (" << x << ", "  << y << ") = " << firstSobelValue << ", ";
            //	//std::cout << "(" << x + previousHalfOffsetX << ",  "<< y + previousHalfOffsetY << ") = " << firstPreviousHalfSobelValue << ", ";
            //	//std::cout << "(" << x + nextHalfOffsetX << ",  "<< y + nextHalfOffsetY << ") = " << firstNextHalfSobelValue << std::endl;
            //	std::cout << d << ", " << firstSobelValue << ", " << firstMinSobelValue << ", " << firstMaxSobelValue << ", ";
            //	std::cout << secondSobelValue << ", " << secondMinSobelValue << ", " << secondMaxSobelValue << ", ";
            //	std::cout << sobelCost << std::endl;
            //}

            //int sobelCost = std::abs(firstSobelValue - secondSobelValue);
            int censusCost = _mm_popcnt_u32(static_cast<unsigned int>(firstCensusCode^secondCensusCode));

            if (stereoDisparityImage(x, y) == 0)
            {
                pixelwiseCostRow_[disparityTotal_*x + d] = static_cast<unsigned char>(((sobelCost + censusCost*flowCensusWeight)*flowCostWeight)*(1 + flowCostWeight));
            }
            else
            {
                pixelwiseCostRow_[disparityTotal_*x + d] += static_cast<unsigned char>((sobelCost + censusCost*flowCensusWeight)*flowCostWeight);
            }
            //pixelwiseCostRow_[disparityTotal_*x + d] = static_cast<unsigned char>((sobelCost + censusCost*flowCensusWeight)*flowCostWeight);
        }
    }
}

double StereoFlowGCCostCalculator::directionalSobel(const rev::Image<short>& horizontalSobelImage,
        const rev::Image<short>& verticalSobelImage,
        const double epipolarDirectionX,
        const double epipolarDirectionY,
        const int x, const int y) const
{
    int horizontalValue = horizontalSobelImage(x, y);
    int verticalValue = verticalSobelImage(x, y);
    double sobelValue = horizontalValue*epipolarDirectionX + verticalValue*epipolarDirectionY;

    return sobelValue;
}

int StereoFlowGCCostCalculator::subpixelInterpolatedCappedSobel(const rev::Image<short>& horizontalSobelImage,
        const rev::Image<short>& verticalSobelImage,
        const unsigned char* capTable,
        const double epipolarDirectionX,
        const double epipolarDirectionY,
        const double x, const double y) const
{
    int x0 = static_cast<int>(floor(x));
    if (x0 < 0) x0 = 0;
    int y0 = static_cast<int>(floor(y));
    if (y0 < 0) y0 = 0;
    if (x0 >= width_) x0=width_-1;
    if (y0 >= height_) y0=height_-1;

    int x1 = x0 + 1;
    if (x1 >= width_) x1 = width_ - 1;
    int y1 = y0 + 1;
    if (y1 >= height_) y1 = height_ - 1;

    double sobel00 = directionalSobel(horizontalSobelImage, verticalSobelImage,
                                      epipolarDirectionX, epipolarDirectionY,
                                      x0, y0);
    double sobel01 = directionalSobel(horizontalSobelImage, verticalSobelImage,
                                      epipolarDirectionX, epipolarDirectionY,
                                      x0, y1);
    double sobel10 = directionalSobel(horizontalSobelImage, verticalSobelImage,
                                      epipolarDirectionX, epipolarDirectionY,
                                      x1, y0);
    double sobel11 = directionalSobel(horizontalSobelImage, verticalSobelImage,
                                      epipolarDirectionX, epipolarDirectionY,
                                      x1, y1);

    double rx = x - x0;
    double ry = y - y0;

    double sobelValue = (1.0 - rx)*(1.0 - ry)*sobel00 + (1.0 - rx)*ry*sobel01 + rx*(1.0 - ry)*sobel10 + rx*ry*sobel11;

    return capTable[static_cast<int>(sobelValue + 0.5)];
}
