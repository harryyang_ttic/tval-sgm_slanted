#include <iostream>
#include <string>
#include <time.h>
#include <cmdline.h>
#include <revlib.h>
#include "CameraMotion.h"
#include "TriMatchedPoint.h"

struct ParameterEstVzDRatio {
	bool verbose;
	std::string disparityImageFilename;
	std::string flowImageFilename;
	std::string calibrationFilename;
	std::string fundamentalMatrixFilename;
	std::string outputCameraFilename;
};

// Prototype declaration
ParameterEstVzDRatio parseCommandline(int argc, char* argv[]);

ParameterEstVzDRatio parseCommandline(int argc, char* argv[]) {
	// Make command parser
	cmdline::parser commandParser;
	commandParser.add("verbose", 'v', "verbose");
	commandParser.add("help", 'h', "display this message");
	commandParser.set_program_name("estalpha");
	commandParser.footer("disparity_image flow_image calibration_file fundamental_matrix output_camera_file");

	// Parse command line
	bool isCorrectCommand = commandParser.parse(argc, argv);
	if (!isCorrectCommand) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommand || commandParser.exist("help") || commandParser.rest().size() < 5) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	// Set program parameters
	ParameterEstVzDRatio parameters;
	// Verbose flag
	parameters.verbose = commandParser.exist("verbose");
	// Input files
	parameters.disparityImageFilename = commandParser.rest()[0];
	parameters.flowImageFilename = commandParser.rest()[1];
	parameters.calibrationFilename = commandParser.rest()[2];
	parameters.fundamentalMatrixFilename = commandParser.rest()[3];
	parameters.outputCameraFilename = commandParser.rest()[4];

	return parameters;
}

int main(int argc, char* argv[]) {
	ParameterEstVzDRatio parameters = parseCommandline(argc, argv);

	if (parameters.verbose) {
		std::cerr << std::endl;
		std::cerr << "Disparity image:    " << parameters.disparityImageFilename << std::endl;
		std::cerr << "Flow image:         " << parameters.flowImageFilename << std::endl;
		std::cerr << "Calibration file:   " << parameters.calibrationFilename << std::endl;
		std::cerr << "Fundamental matrix: " << parameters.fundamentalMatrixFilename << std::endl;
		std::cerr << "Output camera file: " << parameters.outputCameraFilename << std::endl;
		std::cerr << std::endl;
	}

	try {

		clock_t t1, t2;
		
		rev::Image<unsigned short> disparityImage = rev::read16bitImageFile(parameters.disparityImageFilename);
		rev::Image<unsigned short> flowImage = rev::read16bitImageFile(parameters.flowImageFilename);
		int width = disparityImage.width();
		int height = disparityImage.height();

		t1 = clock();
		CameraMotion cameraMotion;
		cameraMotion.initialize(parameters.calibrationFilename, parameters.fundamentalMatrixFilename);

		std::vector<TriMatchedPoint> matchedPoints;
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (disparityImage(x, y) == 0 || flowImage(x, y, 2) == 0) continue;

				double stereoDisparity = static_cast<double>(disparityImage(x, y))/256.0;
				double flowX = static_cast<double>(flowImage(x, y, 0) - 32768)/64.0;
				double flowY = static_cast<double>(flowImage(x, y, 1) - 32768)/64.0;

				TriMatchedPoint newCorrespondence;
				newCorrespondence.set(x, y, x - stereoDisparity, y, x + flowX, y + flowY);
				matchedPoints.push_back(newCorrespondence);
			}
		}

		cameraMotion.estimateRatioVzDisparity(matchedPoints);
		t2 = clock();
		if (parameters.verbose) std::cout << "Time: " << static_cast<double>(t2 - t1)/CLOCKS_PER_SEC << std::endl;

		cameraMotion.writeCameraMotionFile(parameters.outputCameraFilename);

	} catch (const rev::Exception& revException) {
		std::cerr << "Error [" << revException.functionName() << "]: " << revException.message() << std::endl;
		exit(1);
	}
}
