#ifndef SIFT_MATCHER_H
#define SIFT_MATCHER_H

#include <vector>
#include <revlib.h>
#include "MatchedPoint.h"

class SiftMatcher {
public:
    SiftMatcher();
    SiftMatcher(const bool useRootSift, const double bestRatioThreshold);
    
    void setParameter(const bool useRootSift, const double bestRatioThreshold);
    
    void match(const rev::Image<unsigned char>& firstImage,
               const rev::Image<unsigned char>& secondImage,
               std::vector<MatchedPoint>& matchedPoints) const;
    void match(const std::vector<rev::Keypoint>& firstKeypoints,
               const std::vector< std::vector<unsigned char> >& firstDescriptors,
               const std::vector<rev::Keypoint>& secondKeypoints,
               const std::vector< std::vector<unsigned char> >& secondDescriptors,
               std::vector<MatchedPoint>& matchedPoints) const;

private:
    struct MatchedIndices {
        int firstIndex;
        int secondIndex;
        double score;
        
        bool operator<(const MatchedIndices& comparisonMatchedIndices) const {
            return (this->score < comparisonMatchedIndices.score);
        }
    };
    
    void convertToFloatDescriptor(const std::vector< std::vector<unsigned char> >& ucharDescriptor,
                                  std::vector< std::vector<float> >& floatDescriptors) const;
    void convertToRootSift(const std::vector< std::vector<unsigned char> >& siftDescriptors,
                           std::vector< std::vector<float> >& rootSiftDescriptors) const;
    void matchKeypoint(const std::vector< std::vector<float> >& firstDescriptors,
                       const std::vector< std::vector<float> >& secondDescriptors,
                       std::vector<MatchedIndices>& matchedIndices) const;
    void setMatchedPoints(const std::vector<rev::Keypoint>& firstKeypoints,
                          const std::vector<rev::Keypoint>& secondKeypoints,
                          const std::vector<MatchedIndices>& matchedIndices,
                          std::vector<MatchedPoint>& matchedPoints) const;
    
    bool useRootSift_;
    double bestRatioThreshold_;
};

#endif
