#ifndef FUNDAMENTAL_MATRIX_H
#define FUNDAMENTAL_MATRIX_H

#include <string>
#include <vector>
#include <eigen3/Eigen/Dense>
#include <revlib.h>
#include "MatchedPoint.h"

class FundamentalMatrix {
public:
    FundamentalMatrix();
    
    void setRobustEstimatorParameters(const double confidenceLevel, const double inlierThreshold = 1.0);
    
    void estimateSelectionLMedS(const std::vector<MatchedPoint>& matchedPoints);
    void estimateLMedS(const std::vector<MatchedPoint>& matchedPoints);
    void estimate8PointAlgorithm(const std::vector<MatchedPoint>& matchedPoints);
    
    void estimateEpipolarSearchDirection(const int imageWidth, const int imageHeight,
                                         const std::vector<MatchedPoint>& matchedPoint);
    
    Eigen::Matrix3d matrixData() const { return matrixData_; }
    int epipolarSearchDirectionFlag() const { return epipolarSearchDirectionFlag_; }
    void computeFirstEpipolarLine(const double secondX, const double secondY,
                                  Eigen::Vector3d& firstEpipolarLine) const;
    void computeSecondEpipolarLine(const double firstX, const double firstY,
                                   Eigen::Vector3d& secondEpipolarLine) const;
    void computeFirstEpipole(double& firstEpipoleX, double& firstEpipoleY) const;
    void computeSecondEpipole(double& secondEpipoleX, double& secondEpipoleY) const;
    
    void computeRotationFlowParameter(const int imageWidth, const int imageHeight,
                                      Eigen::VectorXd& rotationFlowParameter) const;
    
    void readFundamentalMatrixFile(const std::string filename);
    void writeFundamentalMatrixFile(const std::string filename) const;
    
private:
    void selectMatchedPoint(const std::vector<MatchedPoint>& matchedPoints,
                            const double pointDistanceThreshold,
                            std::vector<MatchedPoint>& selectedPoints) const;
    void extractInlierPoint(const std::vector<MatchedPoint>& matchedPoints,
                            const std::vector<bool>& inlierFlags,
                            std::vector<MatchedPoint>& inlierMatchedPoints) const;
    void estimateLMedS(const std::vector<MatchedPoint>& matchedPoints,
                       Eigen::Matrix3d& fundamentalMatrix,
                       std::vector<bool>& inlierFlags);
    void compute8PointAlgorithm(const std::vector<MatchedPoint>& matchedPoints,
                                Eigen::Matrix3d& fundamentalMatrix) const;
    void randomDrawMatchedPoints(const std::vector<MatchedPoint>& matchedPoints,
                                 const int drawTotal,
                                 rev::RandomNumberGenerator& randomGenertor,
                                 std::vector<MatchedPoint>& sampledPoints) const;
    void compute7PointAlgorithm(const std::vector<MatchedPoint>& matchedPoints,
                                std::vector<Eigen::Matrix3d>& fundamentalMatrices) const;
    void computeEpipolarSquareErrors(const std::vector<MatchedPoint>& matchedPoints,
                                     const Eigen::Matrix3d& estimatedFundamentalMatrix,
                                     std::vector<double>& epipolarSquareErrors) const;
    int countInliers(const std::vector<double>& epipolarSquareErrors,
                     std::vector<bool>& inlierFlags) const;
    void solveCubicEquation(const std::vector<double>& coefficients,
                            std::vector<double>& roots) const;

    double confidenceLevel_;
    double inlierThreshold_;
    Eigen::Matrix3d matrixData_;
    unsigned char epipolarSearchDirectionFlag_;
};

#endif
