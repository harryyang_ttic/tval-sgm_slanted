#pragma once

#include <revlib.h>

class SobelFilter {
public:
	SobelFilter() {}

	void filterVerticalHorizontal(const rev::Image<unsigned char>& inputImage,
		rev::Image<short>& horizontalSobelImage,
		rev::Image<short>& verticalSobelImage) const;

private:
	void filterSSE(const rev::Image<unsigned char>& image, rev::Image<short>& horizontalFilteredImage) const;
	void verticalFilter(const rev::Image<unsigned char>& image, rev::Image<short>& verticalFilteredImage) const;
	void convolveVerticalKernel(const unsigned char* imageData, const int width, const int height, short* output) const;
	void convolveHorizontalKernel(short* input, const int width, const int height, short* output) const;
};
