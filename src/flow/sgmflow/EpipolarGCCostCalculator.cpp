#include "EpipolarGCCostCalculator.h"
#include "SobelFilter.h"
#include "CensusTransform.h"

const double EPIPOLARGCCOSTCALCULATOR_DEFAULT_MAX_VZ_RATIO = 0.3;
const int EPIPOLARGCCOSTCALCULATOR_DEFAULT_DISCRETIZATION_TOTAL = 256;
const int EPIPOLARGCCOSTCALCULATOR_DEFAULT_SOBEL_CAP_VALUE = 15;
const int EPIPOLARGCCOSTCALCULATOR_DEFAULT_CENSUS_WINDOW_RADIUS = 2;
const int EPIPOLARGCCOSTCALCULATOR_DEFAULT_CENSUS_WEIGHT_FACTOR = 1.0/2.0;
const int EPIPOLARGCCOSTCALCULATOR_DEFAULT_AGGREGATION_WINDOW_RADIUS = 2;

EpipolarGCCostCalculator::EpipolarGCCostCalculator() : maxVZRatio_(EPIPOLARGCCOSTCALCULATOR_DEFAULT_MAX_VZ_RATIO),
	discretizationTotal_(EPIPOLARGCCOSTCALCULATOR_DEFAULT_DISCRETIZATION_TOTAL),
	sobelCapValue_(EPIPOLARGCCOSTCALCULATOR_DEFAULT_SOBEL_CAP_VALUE),
	censusWindowRadius_(EPIPOLARGCCOSTCALCULATOR_DEFAULT_CENSUS_WINDOW_RADIUS),
	censusWeightFactor_(EPIPOLARGCCOSTCALCULATOR_DEFAULT_CENSUS_WEIGHT_FACTOR),
	aggregationWindowRadius_(EPIPOLARGCCOSTCALCULATOR_DEFAULT_AGGREGATION_WINDOW_RADIUS) {}

EpipolarGCCostCalculator::EpipolarGCCostCalculator(const double maxVZRatio, const int discretizationTotal,
												   const int sobelCapValue,
												   const int censusWindowRadius, const double censusWeightFactor,
												   const int aggregationWindowRadius)
{
	setParameter(maxVZRatio, discretizationTotal, sobelCapValue, censusWindowRadius, censusWeightFactor, aggregationWindowRadius);
}

void EpipolarGCCostCalculator::setParameter(const double maxVZRatio, const int discretizationTotal,
											const int sobelCapValue,
											const int censusWindowRadius, const double censusWeightFactor,
											const int aggregationWindowRadius)
{
	// Max value of VZ-ratio
	if (maxVZRatio <= 0.0 || maxVZRatio > 1.0) {
		throw rev::Exception("EpipolarGCCostCalculator::setParameter", "max value of VZ-ratio is out of range");
	}
	maxVZRatio_ = maxVZRatio;

	// The number of discretization
	if (discretizationTotal <= 0 || discretizationTotal%16 != 0) {
		throw rev::Exception("EpipolarGCCostCalculator::setParameter", "the number of discretization must be a multiple of 16");
	}
	discretizationTotal_ = discretizationTotal;

	// Cap value of sobel filter
	sobelCapValue_ = std::max(sobelCapValue, 15);
	sobelCapValue_ = std::min(sobelCapValue_, 127) | 1;

	// Window size of Census transform
	if (censusWindowRadius != 1 && censusWindowRadius != 2) {
		throw rev::Exception("EpipolarGCCostCalculator::setParameter", "window size of census transform must be 1 or 2");
	}
	censusWindowRadius_ = censusWindowRadius;

	// Cost weight of Census
	if (censusWeightFactor < 0) {
		throw rev::Exception("EpipolarGCCostCalculator::setParameter", "weight of census is less than zero");
	}
	censusWeightFactor_ = censusWeightFactor;

	// Window radius for aggregating pixelwise costs
	if (aggregationWindowRadius < 0) {
		throw rev::Exception("EpipolarGCCostCalculator::setParameter", "window size of cost aggregation is less than zero");
	}
	aggregationWindowRadius_ = aggregationWindowRadius;
}
#include <iostream>
void EpipolarGCCostCalculator::computeCostImage(const rev::Image<unsigned char>& firstImage,
												const rev::Image<unsigned char>& secondImage,
												const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
												rev::Image<unsigned short>& costImage)
{
	rev::Image<unsigned char> firstGrayscaleImage = rev::convertToGrayscale(firstImage);
	rev::Image<unsigned char> secondGrayscaleImage = rev::convertToGrayscale(secondImage);
	width_ = firstGrayscaleImage.width();
	height_ = firstGrayscaleImage.height();

	// Sobel filter
	SobelFilter sobel;
	rev::Image<short> firstHorizontalSobelImage, firstVerticalSobelImage;
	sobel.filterVerticalHorizontal(firstGrayscaleImage, firstHorizontalSobelImage, firstVerticalSobelImage);
	rev::Image<short> secondHorizontalSobelImage, secondVerticalSobelImage;
	sobel.filterVerticalHorizontal(secondGrayscaleImage, secondHorizontalSobelImage, secondVerticalSobelImage);

	// Census transform
	CensusTransform census;
	census.setWindowRadius(censusWindowRadius_);
	rev::Image<int> firstCensusImage;
	census.transform(firstGrayscaleImage, firstCensusImage);
	rev::Image<int> secondCensusImage;
	census.transform(secondGrayscaleImage, secondCensusImage);

	// Buffer
	allocateDataBuffer();
	// Look up table for cap of sobel value
	const int tableOffset = 256*4;
	const int tableSize = tableOffset*2 + 256;
	unsigned char convertTable[tableSize];
	for (int i = 0; i < tableSize; ++i) {
		convertTable[i] = static_cast<unsigned char>(std::min(std::max(i - tableOffset, -sobelCapValue_), sobelCapValue_) + sobelCapValue_);
	}
	unsigned char* capTable = convertTable + tableOffset;

	// Compute cost image
	costImage.resize(width_, height_, discretizationTotal_);
	costImage.setTo(0);
	unsigned short* costImageRow = costImage.dataPointer();
	int widthStepCost = costImage.widthStep();
	calcTopRowCost(firstHorizontalSobelImage, firstVerticalSobelImage,
		secondHorizontalSobelImage, secondVerticalSobelImage,
		capTable,
		firstCensusImage, secondCensusImage,
		epipolarFlowGeometry,
		costImageRow);
	costImageRow += widthStepCost;
	calcRowCosts(firstHorizontalSobelImage, firstVerticalSobelImage,
		secondHorizontalSobelImage, secondVerticalSobelImage,
		capTable,
		firstCensusImage, secondCensusImage,
		epipolarFlowGeometry,
		costImageRow, widthStepCost);
}


void EpipolarGCCostCalculator::allocateDataBuffer() {
	int pixelwiseCostRowBufferSize = width_*discretizationTotal_;
	int rowAggregatedCostBufferSize = width_*discretizationTotal_*(aggregationWindowRadius_*2 + 2);

	pixelwiseCostRow_.resize(pixelwiseCostRowBufferSize);
	rowAggregatedCost_.resize(rowAggregatedCostBufferSize);
}

void EpipolarGCCostCalculator::calcTopRowCost(const rev::Image<short>& firstHorizontalSobelImage,
											  const rev::Image<short>& firstVerticalSobelImage,
											  const rev::Image<short>& secondHorizontalSobelImage,
											  const rev::Image<short>& secondVerticalSobelImage,
											  const unsigned char* capTable,
											  const rev::Image<int>& firstCensusImage,
											  const rev::Image<int>& secondCensusImage,
											  const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
											  unsigned short* costImageRow)
{
	for (int rowIndex = 0; rowIndex <= aggregationWindowRadius_; ++rowIndex) {
		int rowAggregatedCostIndex = std::min(rowIndex, height_ - 1)%(aggregationWindowRadius_*2 + 2);
		unsigned short* rowAggregatedCostCurrent = rowAggregatedCost_.pointer() + rowAggregatedCostIndex*width_*discretizationTotal_;

		calcPixelwiseCost(firstHorizontalSobelImage, firstVerticalSobelImage,
			secondHorizontalSobelImage, secondVerticalSobelImage,
			capTable,
			firstCensusImage, secondCensusImage,
			rowIndex, epipolarFlowGeometry);

		memset(rowAggregatedCostCurrent, 0, discretizationTotal_*sizeof(unsigned short));
		// x = 0
		for (int x = 0; x <= aggregationWindowRadius_; ++x) {
			int scale = x == 0 ? aggregationWindowRadius_ + 1 : 1;
			for (int d = 0; d < discretizationTotal_; ++d) {
				rowAggregatedCostCurrent[d] += static_cast<unsigned short>(pixelwiseCostRow_[discretizationTotal_*x + d]*scale);
			}
		}
		// x = 1...width-1
		for (int x = 1; x < width_; ++x) {
			const unsigned char* addPixelwiseCost = pixelwiseCostRow_.pointer()
				+ std::min((x + aggregationWindowRadius_)*discretizationTotal_, (width_ - 1)*discretizationTotal_);
			const unsigned char* subPixelwiseCost = pixelwiseCostRow_.pointer()
				+ std::max((x - aggregationWindowRadius_ - 1)*discretizationTotal_, 0);

			for (int d = 0; d < discretizationTotal_; ++d) {
				rowAggregatedCostCurrent[discretizationTotal_*x + d]
				= static_cast<unsigned short>(rowAggregatedCostCurrent[discretizationTotal_*(x - 1) + d]
				+ addPixelwiseCost[d] - subPixelwiseCost[d]);
			}
		}

		// Add to cost
		int scale = rowIndex == 0 ? aggregationWindowRadius_ + 1 : 1;
		for (int i = 0; i < width_*discretizationTotal_; ++i) {
			costImageRow[i] += rowAggregatedCostCurrent[i]*scale;
		}
	}
}

void EpipolarGCCostCalculator::calcRowCosts(const rev::Image<short>& firstHorizontalSobelImage,
											const rev::Image<short>& firstVerticalSobelImage,
											const rev::Image<short>& secondHorizontalSobelImage,
											const rev::Image<short>& secondVerticalSobelImage,
											const unsigned char* capTable,
											const rev::Image<int>& firstCensusImage,
											const rev::Image<int>& secondCensusImage,
											const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
											unsigned short* costImageRow, const int widthStepCost)
{
	const __m128i registerZero = _mm_setzero_si128();

	for (int y = 1; y < height_; ++y) {
		int addRowIndex = y + aggregationWindowRadius_;
		int addRowAggregatedCostIndex = std::min(addRowIndex, height_ - 1)%(aggregationWindowRadius_*2 + 2);
		unsigned short* addRowAggregatedCost = rowAggregatedCost_.pointer() + width_*discretizationTotal_*addRowAggregatedCostIndex;

		if (addRowIndex < height_) {
			calcPixelwiseCost(firstHorizontalSobelImage, firstVerticalSobelImage,
				secondHorizontalSobelImage, secondVerticalSobelImage,
				capTable,
				firstCensusImage, secondCensusImage,
				addRowIndex, epipolarFlowGeometry);

			memset(addRowAggregatedCost, 0, discretizationTotal_*sizeof(unsigned short));
			// x = 0
			for (int x = 0; x <= aggregationWindowRadius_; ++x) {
				int scale = x == 0 ? aggregationWindowRadius_ + 1 : 1;
				for (int d = 0; d < discretizationTotal_; ++d) {
					addRowAggregatedCost[d] += static_cast<unsigned short>(pixelwiseCostRow_[discretizationTotal_*x + d]*scale);
				}
			}
			// x = 1...width-1
			int subRowAggregatedCostIndex = std::max(y - aggregationWindowRadius_ - 1, 0)%(aggregationWindowRadius_*2 + 2);
			const unsigned short* subRowAggregatedCost = rowAggregatedCost_.pointer()
				+ width_*discretizationTotal_*subRowAggregatedCostIndex;
			const unsigned short* previousCostRow = costImageRow - widthStepCost;
			for (int x = 1; x < width_; ++x) {
				const unsigned char* addPixelwiseCost = pixelwiseCostRow_.pointer()
					+ std::min((x + aggregationWindowRadius_)*discretizationTotal_, (width_ - 1)*discretizationTotal_);
				const unsigned char* subPixelwiseCost = pixelwiseCostRow_.pointer()
					+ std::max((x - aggregationWindowRadius_ - 1)*discretizationTotal_, 0);

				for (int d = 0; d < discretizationTotal_; d += 16) {
					__m128i registerAddPixelwiseLow = _mm_load_si128(reinterpret_cast<const __m128i*>(addPixelwiseCost + d));
					__m128i registerAddPixelwiseHigh = _mm_unpackhi_epi8(registerAddPixelwiseLow, registerZero);
					registerAddPixelwiseLow = _mm_unpacklo_epi8(registerAddPixelwiseLow, registerZero);
					__m128i registerSubPixelwiseLow = _mm_load_si128(reinterpret_cast<const __m128i*>(subPixelwiseCost + d));
					__m128i registerSubPixelwiseHigh = _mm_unpackhi_epi8(registerSubPixelwiseLow, registerZero);
					registerSubPixelwiseLow = _mm_unpacklo_epi8(registerSubPixelwiseLow, registerZero);

					// Low
					__m128i registerAddAggregated = _mm_load_si128(reinterpret_cast<const __m128i*>(addRowAggregatedCost
						+ discretizationTotal_*(x - 1) + d));
					registerAddAggregated = _mm_adds_epi16(_mm_subs_epi16(registerAddAggregated, registerSubPixelwiseLow),
						registerAddPixelwiseLow);
					__m128i registerCost = _mm_load_si128(reinterpret_cast<const __m128i*>(previousCostRow + discretizationTotal_*x + d));
					registerCost = _mm_adds_epi16(_mm_subs_epi16(registerCost,
						_mm_load_si128(reinterpret_cast<const __m128i*>(subRowAggregatedCost + discretizationTotal_*x + d))),
						registerAddAggregated);
					_mm_store_si128(reinterpret_cast<__m128i*>(addRowAggregatedCost + discretizationTotal_*x + d), registerAddAggregated);
					_mm_store_si128(reinterpret_cast<__m128i*>(costImageRow + discretizationTotal_*x + d), registerCost);

					// High
					registerAddAggregated = _mm_load_si128(reinterpret_cast<const __m128i*>(addRowAggregatedCost + discretizationTotal_*(x-1) + d + 8));
					registerAddAggregated = _mm_adds_epi16(_mm_subs_epi16(registerAddAggregated, registerSubPixelwiseHigh),
						registerAddPixelwiseHigh);
					registerCost = _mm_load_si128(reinterpret_cast<const __m128i*>(previousCostRow + discretizationTotal_*x + d + 8));
					registerCost = _mm_adds_epi16(_mm_subs_epi16(registerCost,
						_mm_load_si128(reinterpret_cast<const __m128i*>(subRowAggregatedCost + discretizationTotal_*x + d + 8))),
						registerAddAggregated);
					_mm_store_si128(reinterpret_cast<__m128i*>(addRowAggregatedCost + discretizationTotal_*x + d + 8), registerAddAggregated);
					_mm_store_si128(reinterpret_cast<__m128i*>(costImageRow + discretizationTotal_*x + d + 8), registerCost);
				}
			}
		}

		costImageRow += widthStepCost;
	}
}

void EpipolarGCCostCalculator::calcPixelwiseCost(const rev::Image<short>& firstHorizontalSobelImage,
												 const rev::Image<short>& firstVerticalSobelImage,
												 const rev::Image<short>& secondHorizontalSobelImage,
												 const rev::Image<short>& secondVerticalSobelImage,
												 const unsigned char* capTable,
												 const rev::Image<int>& firstCensusImage,
												 const rev::Image<int>& secondCensusImage,
												 const int y,
												 const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry)
{
	for (int x = 0; x < width_; ++x) {
		double distanceFromEpipole = epipolarFlowGeometry.distanceFromEpipole(x, y);
		double noDisparityPointX = epipolarFlowGeometry.noDisparityPointX(x, y);
		double noDisparityPointY = epipolarFlowGeometry.noDisparityPointY(x, y);
		double epipolarDirectionX = epipolarFlowGeometry.epipolarDirectionX(x, y);
		double epipolarDirectionY = epipolarFlowGeometry.epipolarDirectionY(x, y);

		double firstOriginalSobelValue = directionalSobel(firstHorizontalSobelImage, firstVerticalSobelImage,
			epipolarDirectionX, epipolarDirectionY, x, y);
		int firstSobelValue = capTable[static_cast<int>(firstOriginalSobelValue + 0.5)];

		int firstCensusCode = firstCensusImage(x, y);

		for (int d = 0; d < discretizationTotal_; ++d) {
			double vzRatio = static_cast<double>(d)/discretizationTotal_*maxVZRatio_;
			double disparity = distanceFromEpipole*vzRatio/(1 - vzRatio);
			double secondX = noDisparityPointX + epipolarDirectionX*disparity;
			double secondY = noDisparityPointY + epipolarDirectionY*disparity;
			int secondXInt = static_cast<int>(secondX + 0.5);
			int secondYInt = static_cast<int>(secondY + 0.5);

			if (secondX < 0 || secondXInt >= width_ || secondY < 0 || secondYInt >= height_) {
				if (d > 0) {
					pixelwiseCostRow_[discretizationTotal_*x + d] = pixelwiseCostRow_[discretizationTotal_*x + d - 1];
				} else {
					pixelwiseCostRow_[discretizationTotal_*x + d] = capTable[0] + 16;
				}
				continue;
			}

			int secondSobelValue = subpixelInterpolatedCappedSobel(secondHorizontalSobelImage, secondVerticalSobelImage,
				capTable, epipolarDirectionX, epipolarDirectionY,
				secondX, secondY);
			int secondCensusCode = secondCensusImage(secondXInt, secondYInt);

			int sobelCost = std::abs(firstSobelValue - secondSobelValue);
			int censusCost = _mm_popcnt_u32(static_cast<unsigned int>(firstCensusCode^secondCensusCode));

			pixelwiseCostRow_[discretizationTotal_*x + d] = static_cast<unsigned char>(sobelCost + censusCost*censusWeightFactor_);
		}
	}
}

double EpipolarGCCostCalculator::directionalSobel(const rev::Image<short>& horizontalSobelImage,
												  const rev::Image<short>& verticalSobelImage,
												  const double epipolarDirectionX,
												  const double epipolarDirectionY,
												  const int x, const int y) const
{
	int horizontalValue = horizontalSobelImage(x, y);
	int verticalValue = verticalSobelImage(x, y);
	double sobelValue = horizontalValue*epipolarDirectionX + verticalValue*epipolarDirectionY;

	return sobelValue;
}

int EpipolarGCCostCalculator::subpixelInterpolatedCappedSobel(const rev::Image<short>& horizontalSobelImage,
															  const rev::Image<short>& verticalSobelImage,
															  const unsigned char* capTable,
															  const double epipolarDirectionX,
															  const double epipolarDirectionY,
															  const double x, const double y) const
{
	int x0 = static_cast<int>(floor(x));
	int y0 = static_cast<int>(floor(y));
	int x1 = x0 + 1;
	if (x1 >= width_) x1 = width_ - 1;
	int y1 = y0 + 1;
	if (y1 >= height_) y1 = height_ - 1;

	double sobel00 = directionalSobel(horizontalSobelImage, verticalSobelImage,
		epipolarDirectionX, epipolarDirectionY,
		x0, y0);
	double sobel01 = directionalSobel(horizontalSobelImage, verticalSobelImage,
		epipolarDirectionX, epipolarDirectionY,
		x0, y1);
	double sobel10 = directionalSobel(horizontalSobelImage, verticalSobelImage,
		epipolarDirectionX, epipolarDirectionY,
		x1, y0);
	double sobel11 = directionalSobel(horizontalSobelImage, verticalSobelImage,
		epipolarDirectionX, epipolarDirectionY,
		x1, y1);

	double rx = x - x0;
	double ry = y - y0;

	double sobelValue = (1.0 - rx)*(1.0 - ry)*sobel00 + (1.0 - rx)*ry*sobel01 + rx*(1.0 - ry)*sobel10 + rx*ry*sobel11;

	return capTable[static_cast<int>(sobelValue + 0.5)];
}
