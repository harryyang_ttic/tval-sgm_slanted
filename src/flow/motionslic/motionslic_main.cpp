#include <iostream>
#include <string>
#include <cmdline.h>
#include <revlib.h>
#include "CameraMotion.h"
#include "MotionSlic.h"

struct ParameterMotionSlic {
	bool verbose;
	int energyType;  // 0: color(First), 1:color(First)+vz-index(First),
	// 2: color(First)+vz-index(First,Second), 3: color(First,Second)+vz-index(First,Second)
	int superpixelTotal;
	int iterationTotal;
	double compactnessWeight;
	double vzIndexWeight;
	double noVZIndexPenalty;
	std::string firstImageFilename;
	std::string secondImageFilename;
	std::string firstVZIndexImageFilename;
	std::string secondVZIndexImageFilename;
	std::string calibrationFilename;
	std::string fundamentalMatrixFilename;
	std::string outputSegmentImageFilename;
	std::string outputBoundaryImageFilename;
	std::string outputVZIndexImageFilename;
	std::string outputFlowImageFilename;
};

// Prototype declaration
ParameterMotionSlic parseCommandline(int argc, char* argv[]);

ParameterMotionSlic parseCommandline(int argc, char* argv[]) {
	// Make command parser
	cmdline::parser commandParser;
	commandParser.add<std::string>("output", 'o', "output segment file", false, "");
	commandParser.add<int>("type", 't', "energy type (0:color1, 1:color1+vz-index1, 2:color1/2+vz-index1/2, 3:color1/2+vz-index1/2", false, 3);
	commandParser.add<int>("superpixel", 's', "the number of superpixels", false, 1000);
	commandParser.add<int>("iteration", 'i', "the number of iterations", false, 10);
	commandParser.add<double>("compactness", 'c', "compactness weight", false, 3000);
	commandParser.add<double>("vz", 'z', "weight of vz-index term", false, 30);
	commandParser.add<double>("penalty", 'p', "penalty value of vz-index term without vz-index estimation", false, 3);
	commandParser.add("verbose", 'v', "verbose");
	commandParser.add("help", 'h', "display this message");
	commandParser.set_program_name("motionslic");
	commandParser.footer("first_image second_image first_vz-index_image second_vz-index_image calibration_file fundamental_matrix_file");

	// Parse command line
	bool isCorrectCommandline = commandParser.parse(argc, argv);
	// Check arguments
	if (!isCorrectCommandline) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommandline || commandParser.exist("help") || commandParser.rest().size() < 6) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	// Set program parameters
	ParameterMotionSlic parameters;
	// Verbose flag
	parameters.verbose = commandParser.exist("verbose");
	// Energy type
	parameters.energyType = commandParser.get<int>("type");
	// The number of superpixels
	parameters.superpixelTotal = commandParser.get<int>("superpixel");
	// The number of iterations
	parameters.iterationTotal = commandParser.get<int>("iteration");
	// Compactness factor
	parameters.compactnessWeight = commandParser.get<double>("compactness");
	// Disparity weight
	parameters.vzIndexWeight = commandParser.get<double>("vz");
	// Occluded penalty
	parameters.noVZIndexPenalty = commandParser.get<double>("penalty");
	// Input files
	parameters.firstImageFilename = commandParser.rest()[0];
	parameters.secondImageFilename = commandParser.rest()[1];
	parameters.firstVZIndexImageFilename = commandParser.rest()[2];
	parameters.secondVZIndexImageFilename = commandParser.rest()[3];
	parameters.calibrationFilename = commandParser.rest()[4];
	parameters.fundamentalMatrixFilename = commandParser.rest()[5];
	// Output files
	std::string outputSegmentImageFilename = commandParser.get<std::string>("output");
	if (outputSegmentImageFilename == "") {
		outputSegmentImageFilename = parameters.firstImageFilename;
		size_t dotPosition = outputSegmentImageFilename.rfind('.');
		if (dotPosition != std::string::npos) outputSegmentImageFilename.erase(dotPosition);
		outputSegmentImageFilename += "_slic.png";
	}
	parameters.outputSegmentImageFilename = outputSegmentImageFilename;
	std::string outputBoundaryImageFilename = outputSegmentImageFilename;
	size_t dotPosition = outputBoundaryImageFilename.rfind('.');
	if (dotPosition != std::string::npos) outputBoundaryImageFilename.erase(dotPosition);
	std::string outputVZIndexImageFilename = outputBoundaryImageFilename;
	std::string outputFlowImageFilename = outputBoundaryImageFilename;
	outputBoundaryImageFilename += "_boundary.png";
	outputVZIndexImageFilename += "_vz.png";
	outputFlowImageFilename += "_flow.png";
	parameters.outputBoundaryImageFilename = outputBoundaryImageFilename;
	parameters.outputVZIndexImageFilename = outputVZIndexImageFilename;
	parameters.outputFlowImageFilename = outputFlowImageFilename;

	return parameters;
}

int main(int argc, char* argv[]) {
	// Parse command line
	ParameterMotionSlic parameters = parseCommandline(argc, argv);

	// Output parameters
	if (parameters.verbose) {
		std::cerr << std::endl;
		std::cerr << "First image:           " << parameters.firstImageFilename << std::endl;
		std::cerr << "Second image:          " << parameters.secondImageFilename << std::endl;
		std::cerr << "First VZ-index image:  " << parameters.firstVZIndexImageFilename << std::endl;
		std::cerr << "Second VZ-index image: " << parameters.secondVZIndexImageFilename << std::endl;
		std::cerr << "Calibration file:      " << parameters.calibrationFilename << std::endl;
		std::cerr << "Fundamental matrix:    " << parameters.fundamentalMatrixFilename << std::endl;
		std::cerr << "Output segment image:  " << parameters.outputSegmentImageFilename << std::endl;
		std::cerr << "Output boundary image: " << parameters.outputBoundaryImageFilename << std::endl;
		std::cerr << "Output VZ-index image: " << parameters.outputVZIndexImageFilename << std::endl;
		std::cerr << "Output flow image:     " << parameters.outputFlowImageFilename << std::endl;
		std::cerr << "   energy type:          ";
		if (parameters.energyType == 0) std::cerr << "color(first)" << std::endl;
		else if (parameters.energyType == 1) std::cerr << "color(first) + vz-index(first)" << std::endl;
		else if (parameters.energyType == 2) std::cerr << "color(first) + vz-index(first+second)" << std::endl;
		else std::cerr << "color(first+second) + vz-index(first+second)" << std::endl;
		std::cerr << "   #superpixels:        " << parameters.superpixelTotal << std::endl;
		std::cerr << "   #iterations:         " << parameters.iterationTotal << std::endl;
		std::cerr << "   compactness weight:  " << parameters.compactnessWeight << std::endl;
		std::cerr << "   vz-index weight:     " << parameters.vzIndexWeight << std::endl;
		std::cerr << "   no vz-index penalty: " << parameters.noVZIndexPenalty << std::endl;
		std::cerr << std::endl;
	}

	try {
		// Open inputs
		rev::Image<unsigned char> firstImage = rev::readImageFile(parameters.firstImageFilename);
		rev::Image<unsigned char> secondImage = rev::readImageFile(parameters.secondImageFilename);
		rev::Image<unsigned short> firstVZImage = rev::read16bitImageFile(parameters.firstVZIndexImageFilename);
		rev::Image<unsigned short> secondVZImage = rev::read16bitImageFile(parameters.secondVZIndexImageFilename);
		CameraMotion cameraMotion;
		cameraMotion.initialize(parameters.calibrationFilename, parameters.fundamentalMatrixFilename);

		// MotionSLIC
		MotionSlic motionSlic;
		motionSlic.setEnergyType(parameters.energyType);
		motionSlic.setIterationTotal(parameters.iterationTotal);
		motionSlic.setWeightParameters(parameters.compactnessWeight, parameters.vzIndexWeight, parameters.noVZIndexPenalty);
		// Perform superpixel segmentation
		rev::Image<unsigned short> segmentImage;
		rev::Image<unsigned short> segmentVZIndexImage, segmentFlowImage;
		motionSlic.segment(parameters.superpixelTotal,
			firstImage, secondImage, firstVZImage, secondVZImage, cameraMotion,
			segmentImage, segmentVZIndexImage, segmentFlowImage);

		// Output
		rev::write16bitImageFile(parameters.outputSegmentImageFilename, segmentImage);
		rev::Image<unsigned char> boundaryImage = rev::drawSegmentBoundary(firstImage, segmentImage);
		rev::writeImageFile(parameters.outputBoundaryImageFilename, boundaryImage);
		rev::write16bitImageFile(parameters.outputVZIndexImageFilename, segmentVZIndexImage);
		rev::write16bitImageFile(parameters.outputFlowImageFilename, segmentFlowImage);

	} catch (const rev::Exception& revException) {
		std::cerr << "Error [" << revException.functionName() << "]: " << revException.message() << std::endl;
		exit(1);
	}
}
