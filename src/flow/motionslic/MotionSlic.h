#pragma once

#include <revlib.h>
#include "CameraMotion.h"
#include "CalibratedEpipolarFlowGeometry.h"

class MotionSlic {
public:
	MotionSlic();

	void setEnergyType(const int energyType);
	void setIterationTotal(const int iterationTotal);
	void setWeightParameters(const double compactnessWeight,
		const double vzIndexWeight,
		const double noVZIndexPenalty);
	void setVZRatioParameters(const double maxVZRatio, const int discretizationTotal);

	void segment(const int superpixelTotal,
		const rev::Image<unsigned char>& firstImage,
		const rev::Image<unsigned char>& secondImage,
		const rev::Image<unsigned short>& firstVZIndexShortImage,
		const rev::Image<unsigned short>& secondVZIndexShortImage,
		const CameraMotion& cameraMotion,
		rev::Image<unsigned short>& segmentImage,
		rev::Image<unsigned short>& segmentVZIndexImage,
		rev::Image<unsigned short>& segmentFlowImage);

private:
	struct LabXYInd {
		LabXYInd() {
			color[0] = 0;  color[1] = 0;  color[2] = 0;
			position[0] = 0;  position[1] = 0;
			vzIndexPlane[0] = 0;  vzIndexPlane[1] = 0;  vzIndexPlane[2] = -1;
		}

		double color[3];
		double position[2];
		double vzIndexPlane[3];
	};
	struct VZIndexPixel {
		double x;
		double y;
		double vzIndex;
	};

	void setColorAndDisparity(const rev::Image<unsigned char>& firstImage,
		const rev::Image<unsigned char>& secondImage,
		const rev::Image<unsigned short>& firstVZIndexShortImage,
		const rev::Image<unsigned short>& secondVZIndexShortImage);
	void initializeSeeds(const int superpixelTotal);
	void performSegmentation(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry);
	void enforceLabelConnectivity();
	void makeSegmentImage(rev::Image<unsigned short>& segmentImage) const;
	void makeVZIndexAndFlowImage(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
		rev::Image<unsigned short>& vzIndexImage,
		rev::Image<unsigned short>& flowImage);

	void checkInputImages(const rev::Image<unsigned char>& firstImage,
		const rev::Image<unsigned char>& secondImage,
		const rev::Image<unsigned short>& firstVZIndexShortImage,
		const rev::Image<unsigned short>& secondVZIndexShortImage) const;
	void setLabImages(const rev::Image<unsigned char>& firstImage,
		const rev::Image<unsigned char>& secondImage);
	void calcFirstLabEdges();
	void setVZIndexImages(const rev::Image<unsigned short>& firstVZIndexShortImage,
		const rev::Image<unsigned short>& secondVZIndexShortImage);
	void setGridSeedPoint(const int superpixelTotal);
	void perturbSeeds();
	void assignLabel(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry);
	void updateSeeds();
	void updateSeedsColorAndPosition();
	void estimateVZIndexPlaneParameter();
	std::vector< std::vector<VZIndexPixel> > makeVZIndexPixelList() const;
	std::vector<double> estimateVZIndexPlaneParameter(const std::vector<VZIndexPixel>& vzIndexPixels) const;
	std::vector<double> estimateVZIndexPlaneParameterRANSAC(const std::vector<VZIndexPixel>& vzIndexPixels) const;
	void solvePlaneEquations(const double x1, const double y1, const double z1, const double d1,
		const double x2, const double y2, const double z2, const double d2,
		const double x3, const double y3, const double z3, const double d3,
		std::vector<double>& planeParameter) const;
	void labelConnectedPixels(const int x,
		const int y,
		const int newLabelIndex,
		std::vector<int>& newLabels,
		std::vector<int>& connectedXs,
		std::vector<int>& connectedYs) const;
	void interpolateVZIndexPlane(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry);


	// Parameter
	int energyType_;
	int iterationTotal_;
	double gridSize_;
	double compactnessWeight_;
	double vzIndexWeight_;
	double noVZIndexPenalty_;
	double maxVZRatio_;
	int discretizationTotal_;

	// Data
	int labelTotal_;
	std::vector<int> labels_;

	// Color and disparity images
	rev::Image<float> firstLabImage_;
	rev::Image<float> secondLabImage_;
	rev::Image<float> firstVZIndexImage_;
	rev::Image<float> secondVZIndexImage_;
	std::vector<double> firstLabEdges_;

	// Superpixel segments
	std::vector<LabXYInd> seeds_;
	int stepSize_;
};
